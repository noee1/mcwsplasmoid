import qbs

Project {
    minimumQbsVersion: "1.7.1"
    name: "MCWS Plasmoid"
    Application {

        property stringList qmlImportPaths: ["/home/mike/kde/usr/lib64/qml","/usr/lib64/qt6/qml"]

        files: [
            "plasmoid/metadata.json",
            "*.md",
            "plasmoid/**/*.qml",
            "plasmoid/**/*.*js",
            "plasmoid/**/*.xml",
            "plasmoid/**/*.png"
        ]
    }
}
