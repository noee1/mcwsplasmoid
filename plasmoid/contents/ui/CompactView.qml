import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import org.kde.plasma.plasmoid
import org.kde.kirigami as Kirigami

import 'controls'
import 'helpers'
import 'theme'

pragma ComponentBehavior: Bound

ListView {
    id: root
    clip: true
    orientation: ListView.Horizontal

    readonly property bool rightJustify: Plasmoid.configuration.rightJustify
    layoutDirection: rightJustify
                     ? Qt.RightToLeft
                     : Qt.LeftToRight

    Component.onCompleted: {
        // At plasmoid start, if connected, we will have already missed connection signals
        // as the Loader is dynamic, so reset explicitly on create...
        if (mcws.isConnected) {
            reset(mcws.getPlayingZoneIndex())
        }
        // ...and wait before enabling the signals from mcws
        event.queueCall(500, () => conn.enabled = true)
    }

    readonly property bool useImageIndicator: Plasmoid.configuration.useImageIndicator
    readonly property bool scrollText: Plasmoid.configuration.scrollTrack

    property int pointSize: Math.floor(height * 0.27)
    property int indHeight: Math.round(height * .8)
    property int itemWidth: Math.round(parent.width
                            / Math.max(mcws.zonesWithPlaylist.count, 2))

    function reset(zonendx) {
        event.queueCall(() => {
            root.model = mcws.zoneModel
            root.currentIndex = zonendx
        })
    }

    signal zoneClicked(int zonendx)
    signal zoneHovered(var info)  // contains general zone/status

    Connections {
        id: conn
        target: mcws
        enabled: false

        function onConnectionStart() { root.model = '' }
        function onConnectionReady(zonendx) { reset(zonendx) }
    }

    component TrackLabel: Marquee {
        align: root.rightJustify ? Text.AlignRight : Text.AlignLeft
        Layout.alignment: root.rightJustify ? Qt.AlignRight : Qt.AlignLeft
        elide: Text.ElideRight
        implicitWidth: tt.width
        padding: 1

        onTextChanged: {
            // wait the default fade duration
            event.queueCall(fadeDuration, () => {
                if (scrollText && playingnowtracks > 0)
                    restart()
            })
        }

        MouseAreaEx {
            onClicked: root.zoneClicked(index)
        }
    }

    component Spacer: Rectangle {
        width: 1
        Layout.fillHeight: true
        color: Kirigami.Theme.disabledTextColor
    }

    delegate: ItemDelegate {
        id: del
        height: root.height
        width: root.itemWidth

        required property int index
        required property int playingnowtracks
        required property var model
        required property string filekey
        required property string nextfilekey
        required property string artist
        required property string zonename
        required property string album
        required property string name
        required property string positiondisplay
        required property string volumedisplay
        required property string nexttrackdisplay

        background: BackgroundTheme {
            anchors.fill: parent
            theme: plasmoidRoot.themeSetup
            active: theme.enabled && Plasmoid.configuration.useImageBackground
                    && model.state !== PlayerState.Stopped
            mainImage: ti.image

            FadeBehavior on active {}
        }

        hoverEnabled: true
        onHoveredChanged: {
            if (hovered) {
                zoneHovered({zone: zonename
                                , status: model.status
                                , pos: positiondisplay.replace(/ /g, '')
                                , vol: volumedisplay
                                , next: nexttrackdisplay
                                , nextfilekey: nextfilekey})
            }
        }

        RowLayout {
            height: parent.height
            width: parent.width

            // playback indicator, only load when player not stopped
            TrackImage {
                id: ti
                implicitHeight: root.indHeight
                implicitWidth: root.indHeight
                Layout.alignment: Qt.AlignVCenter
                Layout.leftMargin: Kirigami.Units.smallSpacing

                Loader {
                    active: root.useImageIndicator && del.model.state !== PlayerState.Stopped
                    asynchronous: true

                    anchors.right: parent.right
                    anchors.bottom: parent.bottom

                    width: Math.floor(ti.width/3)
                    height: Math.floor(ti.height/3)

                    Kirigami.Icon {
                        anchors.fill: parent
                        enabled: true
                        color: del.model.state === PlayerState.Paused
                               ? Kirigami.Theme.disabledTextColor
                               : "#4f9d50"
                        source: {
                            if (del.model.state === PlayerState.Paused)
                                "media-playback-pause-symbolic"
                            else if (del.model.state === PlayerState.Playing)
                                "audio-volume-high"
                            else
                                ""
                        }
                    }
                }

                sourceKey: del.filekey
                imageUtils: mcws.imageUtils
                thumbnail: true
                shadow.size: 5
                shadow.xOffset: 2
                shadow.yOffset: 2
                sourceSize: Qt.size(root.indHeight, root.indHeight)

                MouseAreaEx {
                    onClicked: root.zoneClicked(del.index)
                }
            }

            // track text
            Item {
                id: tt
                Layout.fillHeight: true
                Layout.fillWidth: true
                Layout.rightMargin: Kirigami.Units.smallSpacing

                ColumnLayout {
                    spacing: 0
                    anchors.verticalCenter: tt.verticalCenter

                    opacity: {
                        if (root.useImageIndicator) {
                            return del.model.state !== PlayerState.Stopped ? 1.0 : .6
                        } else {
                            return 1.0
                        }
                    }

                    // track
                    TrackLabel {
                        text: del.playingnowtracks > 0 ? del.name : '<empty playlist>'
                        fontSize: root.pointSize
                        opacity: del.model.state !== PlayerState.Stopped
                                    ? 1
                                    : .4
                    }

                    // artist
                    TrackLabel {
                        text: del.playingnowtracks > 0 ? del.artist : del.zonename
                        fontSize: Math.floor(root.pointSize * .85)
                        opacity: del.model.state !== PlayerState.Stopped
                                    ? 1
                                    : .4
                    }
                }

                MouseAreaEx {
                    id: ma
                    onClicked: root.zoneClicked(del.index)

                    onContainsMouseChanged: {
                        if (del.playingnowtracks > 0)
                            controlsTimer.start()
                    }

                    onWheel: wheel => {
                        wheel.accepted = true
                        del.model.player.setVolume(parseFloat(del.model.volume)
                                   + (wheel.angleDelta.y > 0 ? .1 : -.1))
                    }

                    Timer {
                        id: controlsTimer
                        interval: Kirigami.Units.veryLongDuration * 2
                        onTriggered: {
                            if (ma.containsMouse) {
                                if (!plasmoidRoot.expanded)
                                    controls.opacity = .8
                            } else {
                                controls.opacity = 0
                            }

                            stop()
                        }
                    }

                    // control box
                    Rectangle {
                        id: controls
                        radius: 8
                        color: Kirigami.Theme.backgroundColor
                        implicitHeight: zc.height + Kirigami.Units.smallSpacing
                        implicitWidth: zc.width + Kirigami.Units.largeSpacing
                        anchors.centerIn: parent
                        opacity: 0
                        visible: opacity > 0

                        Behavior on opacity {
                            NumberAnimation {
                                duration: Kirigami.Units.longDuration
                            }
                        }

                        // zone controls
                        RowLayout {
                            id: zc
                            spacing: 0
                            anchors.centerIn: parent

                            PrevButton {}
                            PlayPauseButton {}
                            StopButton {
                                visible: Plasmoid.configuration.showStopButton
                            }
                            NextButton {}

                        }

                    }
                }

            }

        }
    }
}


