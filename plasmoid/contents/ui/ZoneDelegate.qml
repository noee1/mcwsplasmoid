import QtQuick
import QtQuick.Layouts
import QtQuick.Controls
import QtQuick.Window
import org.kde.plasma.components as PComp
import org.kde.kirigami as Kirigami

import 'helpers'
import 'controls'
import 'theme'

pragma ComponentBehavior: Bound

ItemDelegate {
    id: lvDel
    implicitHeight: cl.height
    implicitWidth: ListView.view.width

    readonly property var bkImage: bkgdTheme.currentImage

    required property int index
    required property var model
    required property var player
    required property string filekey
    required property string artist
    required property string zonename
    required property string album
    required property string name
    required property string nexttrackdisplay
    required property bool linked
    required property var linkedzones

    property string streamDisplay

    opacity: ListView.isCurrentItem ? 1 : .35
    Behavior on opacity {
        NumberAnimation { duration: Kirigami.Units.shortDuration }
    }

    onClicked: ListView.view.currentIndex = index

    function zoneClicked(ndx) {
        ListView.view.currentIndex = ndx
    }

    background: BackgroundTheme {
        anchors.fill: parent
        id: bkgdTheme
        theme: plasmoidRoot.themeSetup
        mainImage: ti.image
    }

    // Zone Playback options Menu, lazy create
    // Link menu uses zoneModel Repeater
    Component {
        id: zmComp

        Menu {
            id: zoneMenu

            MenuItem { action: lvDel.player.clearPlayingNow }
            MenuSeparator {}
            Menu {
                title: 'DSP'
                enabled: lvDel.model.state !== PlayerState.Stopped

                onAboutToShow: lvDel.player.getLoudness()

                MenuItem {
                    action: lvDel.player.equalizer
                }
                MenuItem {
                    action: lvDel.player.loudness
                }
            }
            Menu {
                id: linkMenu
                title: "Zone Link"
                enabled: zoneView.count > 1

                // load the link zone model
                // Include all zones except yourself
                // def'n { name: zonename, id: zoneid, linked: bool }
                Component.onCompleted: {
                    mcws.zoneModel.forEach((z,i) => {
                        if (i !== lvDel.index) {
                            linkModel.append({ name: z.zonename
                                           , id: z.zoneid
                                           , linked: false })
                        }
                    })
                }

                // Set link menu settings every show
                onAboutToShow: {
                    let zonelist = lvDel.linkedzones === undefined
                        ? []
                        : lvDel.linkedzones.split(';')

                    linkModel.forEach(z => {
                        z.linked = zonelist.length === 0
                                    ? false
                                    : zonelist.includes(z.id.toString())
                    })
                }

                MenuItem {
                    text: 'Unlink'
                    enabled: lvDel.linked
                    icon.name: 'remove-link'
                    onTriggered: lvDel.player.unLinkZone()
                }
                MenuSeparator {}

                Repeater {
                    id: linkRepeater
                    model: BaseListModel{ id: linkModel }
                    delegate: MenuItem {

                        required property string name
                        required property bool linked
                        required property string id

                        text: name
                        icon.name: linked ? 'edit-link' : ''
                        onTriggered: if (!linked) lvDel.player.linkZone(id)
                    }

                }
            }
            Menu {
                id: audioMenu
                title: "Audio Device"

                onAboutToShow: {
                    // make sure device is current
                    lvDel.player.getAudioDevice()
                }

                Repeater {
                    id: audioDevices
                    model: mcws.audioDevices

                    delegate: MenuItem {
                        text: model.device
                        checkable: true
                        checked: index === lvDel.player.audioDevice
                        autoExclusive: true

                        required property var model
                        required property int index
                        onTriggered: {
                            if (index !== lvDel.player.audioDevice) {
                                lvDel.player.setAudioDevice(index)
                            }
                        }
                    }

                }
            }
        }
    }

    // Stream playback comp
    Component {
        id: streamsComp

        Kirigami.Dialog {
            focus: true
            showCloseButton: false
            title: "Choose a channel to play"

            Component.onCompleted: visible = true
            onClosed: destroy()

            preferredHeight: Math.round(lvDel.height*1.5)

            ListView {
                clip: true
                model: mcws.stationSources
                implicitWidth: lvDel.width

                section {
                    property: "source"
                    delegate: Kirigami.ListSectionHeader {
                        required property string section
                        width: ListView.view.width
                        text: 'Channels @ ' + section
                    }
                }

                delegate: PComp.ToolButton {
                    required property var modelData
                    icon.name: "media-playback-start"
                    implicitWidth: ListView.view.width
                    text: modelData.channel
                    onClicked: {
                        streamDisplay = "%1: %2".arg(modelData.source).arg(modelData.channel)
                        lvDel.player.playRadioStation(modelData)
                        close()
                    }
                }
            }
        }

    }

    // Image and track info layouts
    ColumnLayout {
        id: cl
        width: lvDel.width

        // album art and track info
        RowLayout {

            // cover art
            TrackImage {
                id: ti

                sourceKey: lvDel.filekey
                imageUtils: mcws.imageUtils

                sourceSize: {
                    const s = Math.max(thumbSize, Kirigami.Units.iconSizes.large)
                            * Screen.devicePixelRatio
                    return Qt.size(s, s)
                }

                MouseAreaEx {
                    id: ma
                    onClicked: lvDel.zoneClicked(lvDel.index)

                    // zone controls
                    Rectangle {
                        id: zbox
                        z: 1
                        anchors.bottom: parent.bottom
                        anchors.left: parent.left
                        width: parent.width
                        implicitHeight: Kirigami.Units.iconSizes.medium
                        color: Kirigami.Theme.backgroundColor
                        opacity: ma.containsMouse ? .75 : 0

                        Behavior on opacity {
                            NumberAnimation { duration: Kirigami.Units.longDuration }
                        }

                    }

                    RowLayout {
                        anchors.centerIn: zbox
                        z: 1
                        opacity: ma.containsMouse ? 1 : 0

                        Behavior on opacity {
                            NumberAnimation { duration: Kirigami.Units.longDuration }
                        }

                        ShuffleButton {}

                        RepeatButton {}

                        ToolButton {
                            icon.name: 'phonon-gstreamer'
                            onClicked: streamsComp.createObject(lvDel.parent)
                            ToolTip {
                                text: 'Streaming Stations %1'.arg(streamDisplay)
                            }
                        }
                    }
                }
            }

            // Track Info
            ColumnLayout {
                spacing: Kirigami.Units.smallSpacing
                Layout.maximumHeight: ti.height + Kirigami.Units.largeSpacing

                Item { Layout.fillHeight: true }

                // Track name
                Kirigami.Heading {
                    text: lvDel.name
                    fontSizeMode: Text.VerticalFit
                    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                    Layout.fillWidth: true
                    Layout.maximumHeight: Math.round(ti.height*.4)

                    MouseAreaEx {
                        tipText: lvDel.nexttrackdisplay
                        onClicked: lvDel.zoneClicked(lvDel.index)
                        onPressAndHold: logger.log('Track ' + lvDel.filekey, model.track)
                    }
                    FadeBehavior on text {}
                }

                // Artist
                Kirigami.Heading {
                    text: lvDel.artist
                    level: 4
                    fontSizeMode: Text.VerticalFit
                    elide: Text.ElideRight
                    type: Kirigami.Heading.Type.Secondary
                    Layout.fillWidth: true
                    Layout.maximumHeight: Math.round(ti.height*.3)

                    FadeBehavior on text {}
                }

                // Album
                Kirigami.Heading {
                    text: lvDel.album
                    font.pointSize: Kirigami.Theme.smallFont.pointSize
                    fontSizeMode: Text.VerticalFit
                    elide: Text.ElideRight
                    type: Kirigami.Heading.Type.Secondary
                    Layout.fillWidth: true
                    Layout.maximumHeight: Math.round(ti.height*.3)

                    FadeBehavior on text {}
                }

                Item { Layout.fillHeight: true }

                TrackPosControl {
                    Layout.fillWidth: true
                    showSlider: lvDel.model.state !== PlayerState.Stopped
                }
            }

        }

        // zone name/info & playback controls
        RowLayout {

            // Zone name/options menu
            ToolButton {
                id: zb
                text: lvDel.model.zonename
                icon.name: lvDel.model.linked ? 'edit-link' : ''
                implicitWidth: ti.width
                font: Kirigami.Theme.smallFont

                // zone options menu
                property var zm
                onClicked: {
                    if (!zm)
                        zm = zmComp.createObject(lvDel)
                    zm.popup()
                }

                ToolTip {
                    text: lvDel.model.state !== PlayerState.Stopped
                          ? lvDel.model.audiopath
                          : lvDel.model.status
                }
            }

            // player controls
            Player {
                Layout.fillWidth: true
                showVolumeSlider: plasmoid.configuration.showVolumeSlider
                showStopButton: plasmoid.configuration.showStopButton
            }
        }

    }

}
