import QtQuick
import QtQuick.Effects

MultiEffect {
    autoPaddingEnabled: false
    brightness: 0.4
    saturation: 0.6
    blurEnabled: true
    blur: 1.0
    blurMax: 40
}
