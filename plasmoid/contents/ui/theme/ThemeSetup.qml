import QtQuick

QtObject {
    property bool enabled: false
    property bool dark:    false

    property string config
    property string name

    property string color1
    property string color2

    property bool useCoverArt: name.toLowerCase().includes('cover')
    property bool useDefault: name.toLowerCase().includes('default')

    onConfigChanged: loadConfig()
    onNameChanged:   setColors()
    onDarkChanged:   setColors()

    // list of {string: name, string: c1, string: c2}
    property var list: []

    function setColors() {
        let t = list.find(t => t.name === name)
        if (t) {
            color1 = dark ? Qt.darker(t.c1) : Qt.lighter(t.c1)
            color2 = dark ? Qt.darker(t.c2) : Qt.lighter(t.c2)
        }
    }

    function loadConfig() {
        list.length = 0
        try {
            JSON.parse(config)
                .forEach(t => list.push(t))
            setColors()
        }
        catch (err) {
            console.log('Unable to load theme config: ' + err)
        }
    }

}

