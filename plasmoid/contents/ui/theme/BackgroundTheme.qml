import QtQuick
import QtQuick.Shapes
import '../controls'

Loader {
    id: root
    active: theme.enabled

    property ThemeSetup theme

    property bool lighter:        false
    property bool ignoreDefault:  false
    property bool useDefaultImage: theme.useDefault

    property Image _defaultImage
    property Image mainImage
    property Image currentImage: mainImage

    onUseDefaultImageChanged: {
        if (ignoreDefault) return

        if (useDefaultImage) {
            if (!_defaultImage) {
                const i = defaultComp.createObject(root, {sourceKey: '-1'})
                _defaultImage = i.item
            }
            event.queueCall(() => currentImage = _defaultImage)
        } else {
            currentImage = mainImage
            if (_defaultImage)
                _defaultImage.destroy(500)
        }
    }

    // default image
    Component {
        id: defaultComp

        TrackImage {
            visible: false
            animateLoad: false
            thumbnail: true
            imageUtils: mcws.imageUtils
        }
    }

    // background effects
    Component {
        id: hueComp

        BackgroundHue {
            source: currentImage
            opacity: useDefaultImage | theme.useCoverArt
                        ? (theme.dark ? .4 : 1)
                        : 1
            brightness: lighter
                        ? (useDefaultImage | theme.useCoverArt ? 0.5 : 0.4)
                        : 0.0
        }
    }

    Component {
        id: gradComp

        Rectangle {
            opacity: .75
            gradient: Gradient {
                orientation: Gradient.Horizontal
                GradientStop { position: 0.0; color: theme.color1 }
                GradientStop { position: 0.45; color: theme.color2 }
                GradientStop { position: 1.0; color: "black" }
            }
        }
    }

    sourceComponent: {
        currentImage
            ? (useDefaultImage | theme.useCoverArt)
                ? hueComp
                : gradComp
            : null
    }

}
