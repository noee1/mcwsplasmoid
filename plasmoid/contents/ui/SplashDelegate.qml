import QtQuick
import QtQuick.Layouts
import org.kde.kirigami as Kirigami
import org.kde.plasma.components as Comp

import 'controls'
import 'theme'
import 'helpers/utils.js' as Utils

pragma ComponentBehavior: Bound

// Opacity graphical effects do not work on Windows
// so use a rectangle.  Allows for nice animations too.
Rectangle {
    id: root

    color: 'transparent'

    height: infoRow.height + Kirigami.Units.largeSpacing
    width: infoRow.width + Kirigami.Units.largeSpacing

    anchors.horizontalCenter: splashmode && !animate
                              ? parent.horizontalCenter
                              : undefined
    anchors.verticalCenter: splashmode && !animate
                            ? parent.verticalCenter
                            : undefined

    required property string title
    required property string info1
    required property string info2
    required property string info3
    required property string filekey
    required property bool transparent
    required property bool splashmode
    required property bool fullscreen
    required property bool animate
    required property bool screensaver
    required property int fadeInDuration
    required property int fadeOutDuration
    required property int thumbsize

    property alias splashimg: splashimg

    property real opacityTo: 0.9

    property int dur: Math.min(fadeInDuration, fadeOutDuration) * 10

    // Available area for the panel to exist
    property size availableArea: Qt.size(parent.width, parent.height)

    // callback from viewer to update the model
    property var dataSetter
    function setDataPending(info) {
        d.modelItem = info
        if (animate)
            dataSetterAnimation.start()
    }

    function go() {
        if (splashmode) {
            if (fullscreen | !animate)
                fadeInOut.start()
            else
                if (animate)
                    moveAnimate.reset()
        }
        else {
            if (animate) {
                root.opacity = root.opacityTo
                moveAnimate.reset()
            }
            else {
                x = d.randW()
                y = d.randH()
                fadeInOut.start()
            }
        }
    }

    function stop() {
        fadeOutAnimation.start()
        d.exiting = true
    }

    function reset(info) {
        d.ssFlags = info
    }

    signal splashDone()

    Component.onCompleted: go()

    // private
    QtObject {
        id: d

        // object with values means "reset"
        property var ssFlags
        // object with values means "data pending"
        property var modelItem
        // exit pending for the splash
        property bool exiting: false

        function randW(n) {
            n = n === undefined
                    ? availableArea.width - Math.ceil(root.width/2)
                    : n
            return Math.floor(Math.random() * Math.floor(n))
        }

        function randH(n) {
            n = n === undefined
                    ? availableArea.height - Math.ceil(root.height/2)
                    : n
            return Math.floor(Math.random() * Math.floor(n))
        }

        function pendingDataUpdate() {
            if (modelItem && Utils.isFunction(dataSetter))
                dataSetter(modelItem)
            modelItem = undefined
        }

        function checkForReset(fade) {
            if (ssFlags) {
                let setter = () => {
                    dataSetter(ssFlags)
                    ssFlags = undefined
                    go()
                }
                if (fade) {
                    fadeOutAnimation.start()
                    event.queueCall(fadeOutDuration+1000, setter)
                } else {
                    setter()
                }
                return true
            }

            return false
        }
    }

    BackgroundHue {
        source: !transparent ? splashimg : null
        anchors.fill: !transparent ? parent : undefined
        opacity: !transparent ? 0.65 : 0
    }

    RowLayout {
        id: infoRow
        spacing: Kirigami.Units.smallSpacing*2
        anchors.horizontalCenter: root.horizontalCenter
        anchors.verticalCenter: root.verticalCenter

        TrackImage {
            id: splashimg
            animateLoad: false
            thumbnail: false
            imageUtils: mcws.imageUtils
            sourceKey: root.filekey

            sourceSize: Qt.size(
                Math.max(root.thumbsize, root.fullscreen
                         ? Math.round(root.availableArea.height/4)
                         : (root.screensaver ? 224 : 128))
              , Math.max(root.thumbsize, root.fullscreen
                         ? Math.round(root.availableArea.height/4)
                         : (root.screensaver ? 224 : 128))
            )
        }

        ColumnLayout {
            id: infoColumn
            spacing: 0
            Layout.preferredHeight: splashimg.height + Kirigami.Units.largeSpacing
            Layout.preferredWidth: Math.round(splashimg.width * 2.5) + Kirigami.Units.largeSpacing

            Comp.Label {
                text: root.title
                Layout.fillWidth: true
                Layout.maximumWidth: infoColumn.width
                Layout.fillHeight: true
                enabled: !root.transparent
                elide: Text.ElideRight
                font.pointSize: root.fullscreen | root.screensaver
                                ? Math.round(Kirigami.Theme.defaultFont.pointSize * 2)
                                : Kirigami.Theme.defaultFont.pointSize + 2
            }

            Comp.Label {
                text: root.info1
                Layout.fillWidth: true
                enabled: !root.transparent
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                Layout.maximumWidth: infoColumn.width
                font.pointSize: root.fullscreen | root.screensaver
                                ? Math.round(Kirigami.Theme.defaultFont.pointSize * 1.5)
                                : Kirigami.Theme.defaultFont.pointSize + 1
            }

            Comp.Label {
                text: root.info2
                Layout.fillWidth: true
                Layout.maximumWidth: infoColumn.width
                enabled: !root.transparent
                elide: Text.ElideRight
                font.pointSize: root.fullscreen | root.screensaver
                                ? Math.round(Kirigami.Theme.defaultFont.pointSize * 1.2)
                                : Kirigami.Theme.defaultFont.pointSize
            }

            Comp.Label {
                text: root.info3
                Layout.fillWidth: true
                Layout.maximumWidth: infoColumn.width
                Layout.fillHeight: true
                enabled: !root.transparent
                elide: Text.ElideRight
                font.pointSize: Kirigami.Theme.defaultFont.pointSize-1
            }
        }
    }

    // Move the panels around randomly
    ParallelAnimation {
        id: moveAnimate

        function reset() {
            const wsplit = Math.round((root.availableArea.width+root.width)/2)
            const hsplit = Math.round((root.availableArea.height+root.height)/2)

            // randomize x pos
            xAnim.from = xAnim.to
            if (xAnim.from >= wsplit) { // r2l
                xAnim.to = d.randW(wsplit) - root.width
                if (xAnim.to <= -root.width/2)
                    xAnim.to = -root.width/2
            } else { // l2r
                xAnim.to = d.randW(wsplit) + root.width
                if (xAnim.to >= root.availableArea.width - root.width/3)
                    xAnim.to = root.availableArea.width - root.width/3
            }

            // randomize y pos
            yAnim.from = yAnim.to
            if (yAnim.from >= hsplit) { // b2t
                yAnim.to = d.randH(hsplit) - root.height
                if (yAnim.to <= -root.height/2)
                    yAnim.to = -root.height/2
            } else { // t2b
                yAnim.to = d.randH(hsplit) + root.height
                if (yAnim.to >= root.availableArea.height - root.height/3)
                    yAnim.to = root.availableArea.height - root.height/3
            }

            start()

        }

        XAnimator {
            id: xAnim
            target: root
            duration: root.splashmode ? model.duration : dur/2
            easing.type: Easing.InOutQuad
        }

        YAnimator {
            id: yAnim
            target: root
            duration: root.splashmode ? model.duration : dur
            easing.type: Easing.InOutQuad
        }

        onStopped: {
            if (root.splashmode) {
                fadeOutAnimation.start()
                event.queueCall(fadeOutDuration+500, root.splashDone)
            } else {
                // SS is cancelled
                if (d.exiting) return

                // if ani flags have changed
                if (d.checkForReset(true)) return

                // reset the animation
                event.queueCall(3000, reset)
            }
        }
    }

    // Fade in/out
    SequentialAnimation {
        id: fadeInOut

        OpacityAnimator {
            target: root
            from: 0; to: root.opacityTo
            duration: root.fadeInDuration
        }

        PauseAnimation {
            duration: root.splashmode ? model.duration : dur
        }

        OpacityAnimator {
            target: root
            from: root.opacityTo; to: 0
            duration: root.fadeOutDuration
        }

        onStopped: {
            if (root.splashmode) {
                root.splashDone()
            } else {
                // SS is cancelled
                if (d.exiting) return

                // if ani flags have changed
                if (d.checkForReset()) return

                // handle pending data
                d.pendingDataUpdate()

                // reset the pos, start again
                event.queueCall(1000, () => {
                    root.x = d.randW()
                    root.y = d.randH()
                    start()
                })
            }
        }
    }

    // fade out/set data/fade in
    SequentialAnimation {
        id: dataSetterAnimation

        OpacityAnimator {
            target: root
            from: root.opacityTo; to: 0
            duration: root.fadeOutDuration
        }

        PauseAnimation {
            duration: 500
        }

        ScriptAction { script: { d.pendingDataUpdate() } }

        OpacityAnimator {
            target: root
            from: 0; to: root.opacityTo
            duration: root.fadeInDuration
        }
    }

    OpacityAnimator {
        id: fadeOutAnimation
        target: root
        from: root.opacityTo; to: 0
        duration: root.fadeOutDuration
    }

}
