import QtQuick
import QtQuick.Controls
import QtQuick.Window

import 'controls'
import 'helpers'
import 'theme'

pragma ComponentBehavior: Bound

Item {
    id: root

    property Window background
    property int defaultFadeInDuration: 1500
    property int defaultFadeOutDuration: 1500

    // Splash specific
    property bool showTrackSplash: false
    property bool fullScreenSplash: false
    property bool animateSplash: false
    property int splashDuration: 5000

    // SS specific
    property bool useCoverArt: false
    property bool useMultiScreen: false
    property bool animateSS: false
    property bool transparentSS: false
    property bool screenSaverMode: false

    onScreenSaverModeChanged: {
        if (screenSaverMode) {
            init()
            mcws.zoneModel
                .forEach((zone, ndx) => {
                     if (zone.playingnowtracks > 0)
                         addItem(ndx, zone.filekey,
                                      { animate: animateSS
                                          , fullscreen: false
                                          , screensaver: true
                                          , splashmode: false
                                          , transparent: transparentSS
                                        })
                 })
            background.setImage()
        }
        else {
            background.stopAll()
            event.queueCall(defaultFadeOutDuration, () => {
                panelModel.clear()
                background.destroy()
            })
        }
    }

    onUseMultiScreenChanged:
        if (screenSaverMode && background) background.setDim()

    onUseCoverArtChanged:
        if (screenSaverMode && background) background.setImage()

    onAnimateSSChanged:
        if (screenSaverMode && background) background.resetFlags()

    onTransparentSSChanged:
        if (screenSaverMode && background) background.resetFlags()

    // track item list, indexed to mcws.zonemodel
    readonly property BaseListModel panelModel: BaseListModel {}

    function init() {
        if (!background)
            background = windowComp.createObject(root)
    }

    // return a new track panel model item
    function newItem(zone, zonendx, filekey, flags) {
        return Object.assign(
                  { key: zonendx
                  , filekey: (filekey === undefined ? zone.filekey : filekey)
                  , title: '%1 [%2]'
                        .arg(zone.zonename)
                        .arg(mcws.serverInfo.friendlyname)
                  , info1: zone.name
                  , info2: zone.artist
                  , info3: zone.album
                  , thumbsize: thumbSize
                  , fadeInDuration: defaultFadeInDuration
                  , fadeOutDuration: defaultFadeOutDuration
                  }
                  , flags)
    }

    // Add new panel model item
    function addItem(zonendx, filekey, flags) {
        // Find the ndx for the panel
        // index is info.key
        let zone = mcws.zoneModel.get(zonendx)
        let info = newItem(zone, zonendx, filekey, flags)

        let ndx = panelModel.findIndex(s => s.screensaver && s.key === info.key)
        if (ndx !== -1) {
            // panel found
            background.updatePanel(ndx, info)
            background.setImage(zone.state !== PlayerState.Stopped
                               ? info.filekey
                               : undefined)
        } else {
            // create panel if not found
            panelModel.append(info)
        }
    }

    signal done()

    Component {
        id: windowComp

        Window {
            id: win

            color: 'transparent'
            flags: Qt.FramelessWindowHint | Qt.BypassWindowManagerHint

            // set background image
            function setImage(filekey) {
                if (!useCoverArt) {
                    ti.sourceKey = '-1'
                    return
                }

                if (filekey !== undefined && filekey.length > 0) {
                    ti.sourceKey = filekey
                } else {
                    // Null filekey sent so find a playing zone
                    // If no zones playing, choose a random zone
                    let ndx = mcws.zonesByState(PlayerState.Playing).length
                    if (ndx === 0) { // nothing is playing
                        ndx = Math.floor(Math.random() * mcws.zonesWithPlaylist.count)
                        ti.sourceKey = mcws.zonesWithPlaylist.get(ndx).filekey
                    } else {
                        ndx = mcws.getPlayingZoneIndex()
                        ti.sourceKey = mcws.zoneModel.get(ndx).filekey
                    }
                }
            }

            // update every panel flags
            function resetFlags() {
                for(let i=0, len=panels.count; i<len; ++i)
                    panels.itemAt(i).reset({ animate: animateSS
                                          , transparent: transparentSS
                                          })
            }

            // tell every panel to finish
            function stopAll() {
                for(let i=0, len=panels.count; i<len; ++i)
                    panels.itemAt(i).stop()
            }

            // queue update for a panel
            function updatePanel(ndx, info) {
                panels.itemAt(ndx).setDataPending(info)
            }

            // Some reason, multi screen, can't get the right screen
            // so get it manually, assuming the biggest is "primary"
            function setDim() {
                let maxW = 0, scr
                for (let i=0, len=Qt.application.screens.length; i<len; i++) {
                    const s = Qt.application.screens[i]
                    if (s.width >= maxW) {
                        maxW = s.width
                        scr = s
                    }
                }

                height = screenSaverMode && useMultiScreen
                        ? scr.desktopAvailableHeight
                        : scr.height
                width = screenSaverMode && useMultiScreen
                       ? scr.desktopAvailableWidth
                       : scr.width

                logger.log('Screen %3, %1x%2'.arg(width).arg(height).arg(scr.name))
            }

            // Background cover art
            BackgroundHue {
                anchors.centerIn: parent
                width: Math.round(parent.height*.85)
                height: Math.round(parent.height*.85)
                source: TrackImage {
                    id: ti
                    thumbnail: true
                    visible: false
                    imageUtils: mcws.imageUtils
                }
                opacity: root.showTrackSplash & !root.fullScreenSplash ? 0 : 0.25
            }

            Repeater {
                id: panels
                model: root.panelModel

                onItemRemoved: {
                    if (root.showTrackSplash && root.panelModel.count === 0) {
                        root.background.destroy(100)
                    }
                }

                SplashDelegate {
                    required property int index
                    availableArea: Qt.size(win.width, win.height)

                    dataSetter: data => root.panelModel.set(index, data)

                    // splashMode
                    // track spashers remove themselves from the model
                    onSplashDone: root.panelModel.remove(index)
                }
            }

            // SS menu
            Loader {
                active: root.screenSaverMode

                Menu {
                    id: ssMenu
                    // keep a var so we can react on click
                    // after the menu disappears
                    property bool on: false

                    MenuItem {
                        text: 'Use Cover Art Background'
                        checkable: true
                        checked: root.useCoverArt
                        icon.name: 'emblem-music-symbolic'
                        onTriggered: root.useCoverArt = !root.useCoverArt
                    }
                    MenuItem {
                        text: 'Animate Panels'
                        checkable: true
                        checked: root.animateSS
                        icon.name: 'animation-stage-symbolic'
                        onTriggered: root.animateSS = !root.animateSS
                    }
                    MenuItem {
                        text: 'Transparent Panels'
                        checkable: true
                        checked: root.transparentSS
                        icon.name: 'package-available'
                        onTriggered: root.transparentSS = !root.transparentSS
                    }
                    MenuItem {
                        text: 'Use Multiple Screens'
                        checkable: true
                        checked: root.useMultiScreen
                        icon.name: 'dialog-layers-symbolic'
                        onTriggered: root.useMultiScreen = !root.useMultiScreen
                    }
                    MenuSeparator {}
                    MenuItem {
                        text: 'Stop Screensaver'
                        icon.name: 'process-stop-symbolic'
                        onTriggered: root.screenSaverMode = false
                    }
                }
            }

            MouseAreaEx {
                acceptedButtons: Qt.RightButton | Qt.LeftButton

                focus: true
                Keys.onPressed: root.screenSaverMode = false

                onClicked: mouse => {
                    if (root.showTrackSplash) {
                        root.panelModel.clear()
                        return
                    }

                    if (mouse.button === Qt.LeftButton) {
                        if (ssMenu.on) {
                            ssMenu.on = false
                        }
                        else {
                            root.screenSaverMode = false
                        }
                        return
                    }

                    if (mouse.button === Qt.RightButton) {
                        ssMenu.popup()
                        ssMenu.on = true
                    }
                }
            }

            Component.onCompleted: {
                setDim()
                visible = true
            }

            Component.onDestruction: { root.background = null; done() }
        }
    }

}
