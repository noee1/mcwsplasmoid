import QtQuick
import '../helpers'
import '../helpers/utils.js' as Utils

Item {
    id: root
    property Reader comms
    readonly property alias items: sfm

    // fields model, {field, sortable, searchable, mandatory}
    property BaseListModel mcwsFields

    onMcwsFieldsChanged: init()

    // return an object with all field names as role-proper properties (null string values)
    readonly property var defaultRecordLayout: {
        const ret = {key: ''}
        mcwsFields.forEach(fld => {
                if (fld.mandatory)
                    ret[Utils.toRoleName(fld.field)] = ''
        })
        return ret
    }

    BaseListModel { id: blm }

    BaseSortFilterModel {
        id: sfm
        sortRoleName: Utils.toRoleName(root.sortField)

        filterRowCallback: (i, p) => {
           if (filterString.length === 0)
               return true
           else {
               const o = blm.get(i)
               for (const f in root.searchFields) {
                   if (o[f.toLowerCase()].toLowerCase().includes(filterString))
                       return true
               }

               return false
           }
       }
    }

    property string sortField: ''
    onSortFieldChanged: Qt.callLater(sortReset)

    // Default MCWS search command
    // https://wiki.jriver.com/index.php/Search_Language#Comparison_Operators
    property string searchCmd: 'Files/Search'
                               + (autoShuffle ? '?Shuffle=1&' : '?')
                               + 'query='
    onSearchCmdChanged: {
        if (searchCmd === '')
            clear()
    }

    // MCWS searchable fields struct
    // {Artist: '', Album: '', etc...}
    property var    searchFields: ({})

    // Set this prop and call load or use search()
    property string constraintString: ''

    property bool   autoShuffle: false
    property bool   useFields: true
    property string logicalJoin: 'and'

    signal searchBegin()
    signal searchDone(int count)
    signal sortReset()
    signal debugLogger(string title, var msg, var obj)

    // Initialize the search fields struct
    function init() {
        Utils.simpleClear(searchFields)
        mcwsFields.forEach(fld => {
                               if (fld.searchable)
                                   searchFields[fld.field] = ''
                           })
    }

    // Initiates the search after processing 'val'
    // Use this option or set the constraintString and call load
    function search(val) {
        var constraints = {}
        if (!Utils.isObject(val)) {
            for (const f in searchFields)
                constraints[f] = val
        }
        else
            constraints = val

        constraintString = ''
        if (Object.keys(constraints).length === 0)
            blm.clear()
        else {
            const list = []
            for(const k in constraints) {
                if (constraints[k] !== '')
                    list.push('[%1]=%2'.arg(k).arg(constraints[k]))
            }
            constraintString = list.join(' %1 '.arg(logicalJoin))
            load()
        }

    }

    function removeItem(index) {
        const itemNdx = sfm.mapRowToSource(index)
        if (itemNdx !== -1)
            blm.remove(itemNdx)
        return itemNdx
    }

    function addField(fldObj) {
        if (typeof fldObj !== 'object')
            return false

        const newFld = Object.assign({}, {field: '', sortable: false, searchable: false, mandatory: false}, fldObj)
        if (newFld.field === '')
            return false

        mcwsFields.push(newFld)
        return true
    }

    function setFieldProperty(name, prop, val) {
        const obj = mcwsFields.find((fld) => { return fld.field.toLowerCase() === name.toLowerCase() })
        if (obj) {
            obj[prop] = val
            return true
        }
        return false
    }

    function removeField(name) {
        const ndx = mcwsFields.findIndex((fld) => { return fld.field.toLowerCase === name.toLowerCase() & !fld.mandatory })
        if (ndx !== -1) {
            mcwsFields.splice(ndx,1)
            return true
        }
        return false
    }

    function fieldString() {
        if (!useFields) return ''

        if (mcwsFields.count > 0) {
            const flist = []
            mcwsFields.forEach(fld => flist.push(fld.field))
            return '&Fields=' + flist.join(',').replace(/#/g, '%23')
        } else {
            return '&NoLocalFileNames=1'
        }
    }

    function load() {
        if (comms === undefined) {
            debugLogger('Searcher::load', 'Undefined comms connection')
            searchDone(0)
            return
        }

        searchBegin()
        sfm.sourceModel = null
        blm.clear()

        // append a default record layout to define the model.
        // fixes the case where the first record returned by mcws
        // does not contain values for all of the fields in the constraintString
        if (useFields & mcwsFields.count > 0) {
            blm.append(defaultRecordLayout)
        }

        const cmd = searchCmd
            + (constraintString ?? '')
            + fieldString()
            + '&action=JSON'

        comms.loadModelJSON(cmd, blm)
        .then(cnt => {
            sfm.sourceModel = blm
            searchDone(cnt)
        }).catch(err => searchDone(0))

        debugLogger('Searcher::load', cmd
                    , {searchcmd: searchCmd
                        , fieldstr: fieldString()
                        , constraint: constraintString
                        , sortfield: sortField}
                    )
    }

    function clear() {
        blm.clear()
    }
}
