import QtQuick
import '../helpers'

pragma ComponentBehavior: Bound

Item {
    id: root

    property Reader comms
    readonly property alias items: sfm

    property string queryField: ''
    property string queryFilter: ''
    property string mediaType: 'audio'

    onMediaTypeChanged: blm.load(blm.queryType)

    onQueryFilterChanged: {
        if (queryFilter === '')
            return

        blm.load(LookupValues.QueryType.ByFilter)
    }

    onQueryFieldChanged: {
        if (queryField === '')
            return

        blm.load(LookupValues.QueryType.ByField)
    }

    signal resultsReady(int type, int count)

    function clear() {
        queryField = ''
        queryFilter = ''
        blm.clear()
    }

    function getIcon(field, val) {
        switch (field.toLowerCase()) {
        case 'name':      return 'view-media-track'
        case 'album artist':
        case 'composer':
        case 'artist':    return 'view-media-artist'
        case 'album':     return 'view-media-album-cover'
        case 'genre':     return 'view-media-genre'
        case 'comment':   return 'edit-comment'
        case 'file type': return 'audio-' + val.toLowerCase()
        case 'compression': return 'application-x-compress'
        case 'publisher': return 'view-media-publisher'
        case 'keywords':  return 'view-media-publisher'
        default:          return 'audio-x-generic'
        }

    }

    enum QueryType {
        ByField = 0,
        ByFilter
    }

    BaseSortFilterModel {
        id: sfm
        sortRoleName: 'field'
    }

    // {field, value, iconName}
    BaseListModel {
        id: blm

        property int queryType: LookupValues.QueryType.ByField

        function load(type) {
            blm.clear()
            sfm.sourceModel = null

            queryType = type === undefined
                            ? LookupValues.QueryType.ByField
                            : type

            let query = ''
            if (queryType === LookupValues.QueryType.ByField) {
                root.queryFilter = ''
                query = 'Field=' + root.queryField.replace(/#/g, '%23')
            } else if (queryType === LookupValues.QueryType.ByFilter) {
                queryField = ''
                query = 'Filter=' + queryFilter
            } else {
                logger.logWarning('Invalid Lookup Query Type')
                return
            }

            // custom load to deal with diff xml return results
            comms.__exec('Library/Values'
                         + (mediaType !== ''
                            ? '?Files=[Media Type]=[%1]&'.arg(mediaType)
                            : '?')
                         + query)
            .then(nodes =>
            {
                Array.prototype.forEach.call(nodes, node =>
                {
                    if (node.nodeType === 1 && node.firstChild !== null) {
                        const obj = {}
                        obj.field = node.attributes.length === 0
                                     ? queryField
                                     : node.attributes[0].value.toLowerCase()
                        obj.value = node.firstChild.data
                        obj.iconName = getIcon(obj.field, obj.value)
                        append(obj)
                    }
                })

                sfm.sourceModel = blm
                root.resultsReady(queryType, count)
            })
        }
    }
}
