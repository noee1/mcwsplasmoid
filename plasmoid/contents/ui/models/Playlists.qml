import QtQuick
import QtQuick.Controls
import '../helpers'
import '../helpers/utils.js' as Utils

Item {
    id: root
    property alias comms: tm.comms
    property alias filterString: sf.filterString

    readonly property alias items: sf
    readonly property alias trackModel: tm

    property string filterType: 'All'

    onFilterTypeChanged: {
        if (filterType === '') {
            filterType = 'All'
        }

        if (blm.count === 0)
            blm.load()
        else
            sf.invalidateFilter()
    }

    property list<Action> searchActions: [
        Action {
            text: 'All'
            checkable: true
            icon.name: root.icon(text)
            checked: text === filterType
            onTriggered: filterType = text
        },
        Action {
            text: 'Smartlist'
            checkable: true
            icon.name: root.icon(text)
            checked: text === filterType
            onTriggered: filterType = text
        },
        Action {
            text: 'Playlist'
            checkable: true
            icon.name: root.icon(text)
            checked: text === filterType
            onTriggered: filterType = text
        },
        Action {
            text: 'Group'
            checkable: true
            icon.name: root.icon(text)
            checked: text === filterType
            onTriggered: filterType = text
        }
    ]

    function icon(type) {
        switch (type.toLowerCase()) {
            case 'playlist':    return 'view-media-playlist'
            case 'smartlist':   return 'source-smart-playlist'
            case 'group':       return 'edit-group'
            default:            return 'show-all-effects'
        }
    }

    // load the Playlists from the mcws host
    function load() {
        clear()
        blm.load()
    }

    // load tracks for the playlist plID
    function loadTracks(plID) {
        tm.constraintString = 'playlist=' + plID
        tm.load()
    }

    function clear() {
        tm.clear()
        blm.clear()
    }

    signal debugLogger(string title, var msg, var obj)

    BaseSortFilterModel {
        id: sf
        sourceModel: blm
        property var exclude: ['task --'
                                , 'handheld --'
                                , 'sidecar'
                                , 'image &'
                                , ' am', ' pm']

        filterRowCallback: (i, p) => {
            const pl = blm.get(i)

            // check playlist name for "excluded" strings
            if (exclude.some(ex => pl.name.toLowerCase().includes(ex)))
                return false

            if (filterString.length > 0) {
                if (!pl.name.toLowerCase().includes(filterString.toLowerCase()))
                   return false
            }

            return (filterType === "All")
                    ? pl.type !== "Group"
                    : filterType === pl.type
        }
    }

    BaseListModel {
        id: blm

        /* Playlists/List output
            <Item>
            <Field Name="ID">1572566999</Field>
            <Field Name="Name">Car Radio</Field>
            <Field Name="Path">Car Radio</Field>
            <Field Name="Type">Group</Field>
            </Item> ...
        */

        function load() {
            root.comms.__exec('Playlists/List')
            .then(nodes =>
            {
                Array.prototype.forEach.call(nodes, node =>
                {
                    if (node.nodeType === 1) { // Item
                        const obj = {}
                        Array.prototype.forEach.call(node.childNodes, child =>
                        {
                            if (child.nodeType === 1) // Field
                                obj[Utils.toRoleName(child.attributes[0].value)]
                                         = child.firstChild.data
                        })
                        append(obj)
                    }
                })
            })
        }
    }

    // Tracklist Model for the current playlist (currentIndex)
    Searcher {
        id: tm
        searchCmd: 'Playlist/Files?'
        onDebugLogger: (title, msg, obj) => root.debugLogger(title, msg, obj)
    }
}
