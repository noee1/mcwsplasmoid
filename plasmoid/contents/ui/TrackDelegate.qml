import QtQuick
import QtQuick.Layouts
import QtQuick.Controls
import org.kde.plasma.components as PComp
import org.kde.coreaddons as KCoreAddons
import org.kde.kirigami as Kirigami

import 'controls'
import 'actions'
import 'helpers'
import 'theme'

pragma ComponentBehavior: Bound

ItemDelegate {
    id: detDel
    implicitWidth: ListView.view.width
    implicitHeight: trkDetails.implicitHeight
            + Kirigami.Units.smallSpacing

    required property string key
    required property string mediatype
    required property string mediasubtype
    required property string artist
    required property string album
    required property string genre
    required property string track_
    required property string name
    required property int index
    required property int duration

    background: BackgroundTheme {
        anchors.fill: parent
        theme: plasmoidRoot.themeSetup
        ignoreDefault: true
        mainImage: useDefaultImage
                    ? zoneView.currentItem.bkImage
                    : ti.image
    }

    signal contextClick(int index)

    function toggleExpanded() {
        detDel.state = detDel.state === 'expanded' ? '' : 'expanded'
    }

    function animateTrack() {
        trkAni.start()
    }

    // Animation for "currently playing"
    SequentialAnimation {
        id: trkAni
        PropertyAnimation {
            target: detDel
            property: "scale"
            to: .35
            duration: Kirigami.Units.longDuration
        }
        PropertyAnimation {
            target: detDel
            property: "scale"
            to: 1.5
            duration: Kirigami.Units.longDuration
        }
        PropertyAnimation {
            target: detDel
            property: "scale"
            to: 1
            duration: Kirigami.Units.longDuration
        }
    }

    // Floating controls trigger
    Timer {
        interval: Kirigami.Units.longDuration
        running: mainMa.containsMouse
        onTriggered: floatLoader.active = mainMa.containsMouse
    }

    states: [
        State {
            name: 'expanded'
            PropertyChanges { optLoader.active: true }
        }
    ]

    contentItem: MouseAreaEx {
        id: mainMa
        acceptedButtons: Qt.RightButton | Qt.LeftButton

        onHoveredChanged: {
            if (!containsMouse)
                floatLoader.active = false
        }

        onClicked: mouse => {
            ListView.currentIndex = detDel.index
            if (mouse.button === Qt.RightButton) {
                detDel.contextClick(index)
                detDel.toggleExpanded()
            }
        }

        // Trk Info, floating box and controls, controls are separate item
        // so opacity can be different
        Loader {
            id: floatLoader
            active: false
            z: 1

            sourceComponent: Item {
                x: rl.x
                y: rl.y
                implicitWidth: trkDetails.width
                implicitHeight: rl.height

                visible: false
                Component.onCompleted: visible = true
                VisibleBehavior on visible {}

                Rectangle {
                    id: floatingBox
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.right: parent.right
                    implicitWidth: floatingControls.width + Kirigami.Units.largeSpacing
                    implicitHeight: floatingControls.height + Kirigami.Units.smallSpacing
                    radius: 8
                    color: Kirigami.Theme.backgroundColor
                    opacity: .6

                }

                RowLayout {
                    id: floatingControls
                    anchors.centerIn: floatingBox

                    // play track
                    PlayButton {
                        action: TrackAction { method: 'play' }
                    }

                    // add track
                    AppendButton {
                        action: TrackAction { method: 'add' }
                    }

                    // remove track
                    PComp.ToolButton {
                        visible: !trackView.searchMode
                        action: TrackAction { method: 'remove' }
                        ToolTip { text: 'Remove Track from List' }
                    }

                    PComp.ToolButton {
                        id: expandBtn
                        icon.name: detDel.state === 'expanded' ? 'arrow-up' : 'arrow-down'
                        onClicked: detDel.toggleExpanded()
                        ToolTip { text: 'Artist/Album Options' }
                    }
                }
            }
        }

        ColumnLayout {
            id: trkDetails
            anchors.fill: parent

            // Trk info
            RowLayout {
                id: rl

                // cover art
                TrackImage {
                    id: ti
                    animateLoad: false
                    sourceKey: detDel.key
                    thumbnail: true
                    imageUtils: mcws.imageUtils
                    sourceSize: Qt.size(Math.max(thumbSize/2, 24), Math.max(thumbSize/2, 24))
                    shadow.size: 5
                    shadow.xOffset: 2
                    shadow.yOffset: 2
                    Layout.leftMargin: Kirigami.Units.smallSpacing

                    MouseAreaEx {
                        id: ma
                        acceptedButtons: Qt.RightButton
                        onPressAndHold: {
                            mcws.getTrackDetails(key, trk => logger.log('Track ' + key, trk))
                        }
                    }

                }

                // track details
                ColumnLayout {
                    spacing: 0

                    // Track Name/duration
                    RowLayout {
                        Kirigami.Heading {
                            level: 4
                            elide: Text.ElideRight
                            wrapMode: Text.NoWrap
                            Layout.fillWidth: true
                            Layout.maximumHeight: Math.round(ti.height/2)
                            text: (detDel.mediatype === 'Audio'
                                  ? (!detDel.track_ ? '' : detDel.track_ + '. ') + detDel.name
                                  : '%1 / %2'.arg(detDel.name).arg(detDel.mediatype))
                        }

                        PComp.Label {
                            font: Kirigami.Theme.smallFont
                            text: {
                                if (detDel.duration === undefined) {
                                    return ''
                                }

                                // if track is playing, display playing position
                                let z = zoneView.currentZone
                                if (z && +key === +z.filekey
                                        && (z.state !== PlayerState.Stopped)) {
                                    opacity = 1.0
                                    return '(%1)'.arg(z.positiondisplay.replace(/ /g, ''))
                                }
                                else { // otherwise, track duration
                                    opacity = .7
                                    return KCoreAddons.Format.formatDuration(duration*1000
                                        , duration >= 60*60 ? 0 : KCoreAddons.FormatTypes.FoldHours)
                                }
                            }
                        }
                    }

                    // artist
                    Kirigami.Heading {
                        level: 5
                        type: Kirigami.Heading.Type.Secondary
                        elide: Text.ElideRight
                        wrapMode: Text.NoWrap
                        visible: !abbrevTrackView || detDel.ListView.isCurrentItem
                        Layout.fillWidth: true
                        text: {
                            if (!detDel.mediatype)
                                return ''
                            else if (detDel.mediatype === 'Audio')
                                return detDel.artist ?? ''
                            else if (detDel.mediatype === 'Video')
                                return detDel.mediasubtype ?? ''
                            else return ''
                        }
                    }

                    // album/genre
                    RowLayout {
                        PComp.Label {
                            text: detDel.album ?? ''
                            elide: Text.ElideRight
                            font: Kirigami.Theme.smallFont
                            opacity: .7
                            Layout.fillWidth: true
                            visible: !abbrevTrackView || detDel.ListView.isCurrentItem
                        }

                        PComp.Label {
                            text: detDel.genre ?? ''
                            font: Kirigami.Theme.smallFont
                            opacity: .7
                            elide: Text.ElideRight
                            Layout.maximumWidth: Math.round(detDel.width/4)
                            visible: !abbrevTrackView || detDel.ListView.isCurrentItem
                        }
                    }

                }
            }

            // Track, Artist and Album Options
            Loader {
                id: optLoader
                active: false
                visible: active
                Layout.fillWidth: true

                VisibleBehavior on active { fadeDuration: 100 }

                sourceComponent: ColumnLayout {
                    spacing: 0

                    GroupSeparator{}

                    // album
                    RowLayout {
                        spacing: 0
                        PComp.ToolButton {
                            Layout.fillWidth: true
                            action: AlbumAction {
                                useAText: true
                                icon.name: 'media-playback-start'
                                method: 'play'
                            }
                            PComp.ToolTip {
                                text: 'Play Album'
                                visible: parent.hovered
                            }

                        }
                        AddButton {
                            action: AlbumAction { method: 'addNext' }
                        }
                        AppendButton {
                            action: AlbumAction { method: 'add' }
                        }
                        ShowButton {
                            action: AlbumAction { method: 'show' }
                        }
                    }

                    // artist
                    RowLayout {
                        spacing: 0
                        PComp.ToolButton {
                            Layout.fillWidth: true
                            action: ArtistAction {
                                shuffle: autoShuffle
                                method: 'play'
                                icon.name: 'media-playback-start'
                                useAText: true
                            }
                            PComp.ToolTip {
                                text: 'Play Artist'
                                visible: parent.hovered
                            }
                        }

                        AddButton {
                            action: ArtistAction {
                                method: 'addNext'
                                shuffle: autoShuffle
                            }

                        }
                        AppendButton {
                            action: ArtistAction {
                                method: 'add'
                                shuffle: autoShuffle
                            }
                        }
                        ShowButton {
                            action: ArtistAction {
                                method: 'show'
                                shuffle: autoShuffle
                            }
                        }
                    }

                    // genre
                    RowLayout {
                        spacing: 0
                        PComp.ToolButton {
                            Layout.fillWidth: true
                            action: GenreAction {
                                shuffle: autoShuffle
                                method: 'play'
                                icon.name: 'media-playback-start'
                                useAText: true
                            }
                            PComp.ToolTip {
                                text: 'Play Genre'
                                visible: parent.hovered
                            }
                        }

                        AddButton {
                            action: GenreAction {
                                method: 'addNext'
                                shuffle: autoShuffle
                            }

                        }
                        AppendButton {
                            action: GenreAction {
                                method: 'add'
                                shuffle: autoShuffle
                            }
                        }
                        ShowButton {
                            action: GenreAction {
                                method: 'show'
                                shuffle: autoShuffle
                            }
                        }
                    }

                    // Search results
                    RowLayout {
                        spacing: 0
                        visible: trackView.searchMode & !trackView.showingPlaylist

                        PComp.ToolButton {
                            Layout.fillWidth: true
                            action: PlaySearchListAction { useAText: true }
                            PComp.ToolTip {
                                text: 'Play Search Results'
                                visible: parent.hovered
                            }
                        }

                        AddButton {
                            action: AddSearchListAction {
                                method: 'addNext'
                                shuffle: autoShuffle
                            }
                        }
                        AppendButton {
                            action: AddSearchListAction {
                                shuffle: autoShuffle
                            }
                        }
                    }

                    // Playlist
                    RowLayout {
                        spacing: 0
                        visible: trackView.showingPlaylist

                        PComp.ToolButton {
                            id: pl
                            action: PlayPlaylistAction { useAText: true }
                            hoverEnabled: true
                            Layout.fillWidth: true
                            PComp.ToolTip {
                                text: 'Play ' + pl.text
                                visible: pl.hovered
                            }
                        }

                        AddButton {
                            action: AddPlaylistAction {
                                method: 'addNext'
                                shuffle: autoShuffle
                            }
                        }
                        AppendButton {
                            action: AddPlaylistAction {
                                shuffle: autoShuffle
                            }
                        }
                    }

                }

            }
        }

    }
}
