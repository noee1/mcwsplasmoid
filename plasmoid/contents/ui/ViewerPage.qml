import QtQuick
import org.kde.plasma.components as PComp

PComp.Page {

    readonly property alias viewer: viewer

    signal viewEntered()

    Viewer {
        id: viewer
        anchors.fill: parent
    }
}
