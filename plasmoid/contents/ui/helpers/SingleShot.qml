import QtQuick
import './utils.js' as Utils

QtObject {
    id: root

    function queueCall() {
        if (!arguments)
            return

        const len = arguments.length
        let delay = 0
        let copyargs = []
        let fn

        // check first param fn, run it immediately with args if any
        if (Utils.isFunction(arguments[0])) {
            fn = arguments[0]
            if (len > 1)
                copyargs = [].splice.call(arguments,1)

        // first arg delay, second fn, run with args if any
        } else if (len >= 2) {
            delay = arguments[0]
            fn = Utils.isFunction(arguments[1]) ? arguments[1] : undefined
            if (len > 2)
                copyargs = [].splice.call(arguments,2)

        // noop
        } else {
            console.warn('Invalid arg list: ' + arguments)
            return
        }

        const comp = Qt.createComponent("SSTimer.qml")
        const caller = comp.createObject(root, {fn: fn
                                             , copyargs: copyargs
                                             , interval: delay})
        comp.destroy()
    }

}
