import QtQuick
import QtQuick.Layouts
import QtQuick.Controls
import QtQuick.Window
import org.kde.plasma.extras as Extras
import 'utils.js' as Utils

ApplicationWindow {
    id: root
    title: windowTitle
    height: 800
    width: 600
    x: winPos.includes('e')
            ? screen.virtualX + (screen.width - width)
            : screen.virtualX
    y: winPos.includes('s')
            ? screen.virtualY + (screen.height - height)
            : screen.virtualY

    property QtObject logger
    property string winPos: 'nw'
    property string windowTitle: ''

    header: ToolBar {
        implicitWidth: root.width
        RowLayout {
            width: parent.width
            CheckBox {
                id: autoScroll
                checked: true
                text: 'Auto Scroll'
            }
            ToolButton {
                icon.name: 'edit-clear'
                onClicked: msgModel.clear()
            }
            Item { Layout.fillWidth: true }
            ToolButton {
                icon.name: 'format-align-vertical-top'
                onClicked: msgs.positionViewAtBeginning()
            }
            ToolButton {
                icon.name: 'format-align-vertical-bottom'
                onClicked: msgs.positionViewAtEnd()
            }
        }
    }

    background: Rectangle {
        color: 'black'
//        gradient: Gradient {
//            GradientStop { position: 0; color: "#c1bbf9" }
//            GradientStop { position: 1; color: "black" }
//        }
    }

    Connections {
        target: logger

        function __log(type, title, msg, obj) {
            title = title ?? 'unknown'
            msg = msg ?? ''
            obj = obj ?? ''
            var iconString = 'dialog-positive'
            switch (type) {
                case Logger.Info:
                    iconString = 'dialog-information'
                    break
                case Logger.Error:
                    iconString = 'dialog-error'
                    break
                case Logger.Warning:
                    iconString = 'dialog-warning'
                    break
            }

            msgModel.append({ type: type
                            , title: title
                            , message: Utils.isObject(msg)
                                        ? Utils.stringifyObj(msg)
                                        : msg
                            , object: Utils.isObject(obj)
                                        ? Utils.stringifyObj(obj)
                                        : obj
                            , iconString: iconString
                            })
        }

        function onLogMsg(title, msg, obj) {
            __log(Logger.Info, title, msg, obj)
        }

        function onLogWarning(title, msg, obj) {
            __log(Logger.Warning, title, msg, obj)
        }

        function onLogError(title, msg, obj) {
            __log(Logger.Error, title, msg, obj)
        }
    }

    // {type, title, message, iconString, object}
    ListModel {
        id: msgModel
        onRowsInserted:
            if (autoScroll.checked)
                msgs.positionViewAtEnd()
    }

    ListView {
        id: msgs
        anchors.fill: parent
        model: msgModel
        clip: true

        delegate: Extras.ExpandableListItem {
            title: model.title ?? ''
            subtitle: message ?? ''
            subtitleCanWrap: true
            icon: iconString

            customExpandedViewContent: Component {
                Extras.DescriptiveLabel {
                    text: model.object
                    color: '#18b218'
                    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                }
            }

        }
    }
}
