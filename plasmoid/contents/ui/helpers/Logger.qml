import QtQuick

QtObject {
    id: root
    readonly property bool enabled: __win !== undefined

    property string winTitle: 'Logger'
    property string pos: 'nw'
    property var __win

    enum Type {
        Info = 0,
        Warning,
        Error
    }

    signal logMsg(string title, var msg, var obj)
    signal logWarning(string title, var msg, var obj)
    signal logError(string title, var msg, var obj)

    function init() {
        if (!__win) {
            const winComp = Qt.createComponent('LogWindow.qml')
            __win = winComp.createObject(root, {logger: root
                                             , windowTitle: winTitle
                                             , winPos: pos
                                         })
            __win.show()
            __win.onClosing.connect(() => {
                __win.destroy(100)
                __win = undefined })
        }
        else {
            __win.showNormal()
            __win.raise()
        }
    }

    function close() {
        if (__win) {
            __win.close()
        }
    }

    function log(title, msg, obj) {
        if (__win)
            logMsg(title, msg, obj)
    }

    function warn(title, msg, obj) {
        if (__win)
            logWarning(title, msg, obj)
    }

    function error(title, msg, obj) {
        if (__win)
            logError(title, msg, obj)
    }

}
