import QtQuick
import org.kde.kitemmodels
import 'utils.js' as Utils

// implement sfm
// all funcs map to source
KSortFilterProxyModel {

    function mapRowToSource(row) {
        let mi = mapToSource(index(row, 0))
        return mi.row
    }

    function mapRowFromSource(row) {
        let mi = mapFromSource(sourceModel.index(row, 0))
        return mi.row
    }

    function get(row) {
        if (!sourceModel || row < 0 || row >= sourceModel.count) {
            return null
        }
        return sourceModel.get(mapRowToSource(row))
    }

    function filter(compare) {
        if (!Utils.isFunction(compare))
            return []

        var list = []
        for (let i=0, len = sourceModel.count; i<len; ++i) {
            if (compare(get(i)))
                list.push(i)
        }
        return list
    }

    function findIndex(compare) {
        if (!Utils.isFunction(compare))
            return -1

        for (let i=0, len = sourceModel.count; i<len; ++i) {
            if (compare(get(i)))
                return i
        }
        return -1
    }

    function contains(compare) {
        return (findIndex(compare) !== -1)
    }

    function find(compare) {
        if (!Utils.isFunction(compare))
            return undefined

        for (let i=0, len = sourceModel.rowCount(); i<len; ++i) {
            const item = get(i)
            if (compare(item))
                return item
        }
        return undefined
    }

    function forEach(fun) {
        if (!Utils.isFunction(fun))
            return

        for (let i=0, len = sourceModel.count; i<len; ++i) {
            fun(get(i), i)
        }
    }

    function toArray() {
        const arr = []
        for (let i=0, len = sourceModel.count; i<len; ++i) {
            arr.push(get(i))
        }
        return arr
    }

}

