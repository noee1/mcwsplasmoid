import QtQuick
import 'utils.js' as Utils

ListModel {

    // return array of indicies based on compare
    function filter(compare) {
        if (!Utils.isFunction(compare))
            return []

        const list = []
        for (let i=0, len = count; i<len; ++i) {
            if (compare(get(i)))
                list.push(i)
        }
        return list
    }

    // return index of first obj based on compare
    function findIndex(compare) {
        if (!Utils.isFunction(compare))
            return -1

        for (let i=0, len = count; i<len; ++i) {
            if (compare(get(i)))
                return i
        }
        return -1
    }

    // return bool if model contains compare
    function contains(compare) {
        return (findIndex(compare) !== -1)
    }

    // return first obj based on compare
    function find(compare) {
        if (!Utils.isFunction(compare))
            return undefined

        for (let i=0, len = count; i<len; ++i) {
            const item = get(i)
            if (compare(item))
                return item
        }
        return undefined
    }

    // run func for each obj
    function forEach(func) {
        if (!Utils.isFunction(func))
            return

        for (let i=0, len = count; i<len; ++i) {
            func(get(i), i)
        }
    }

    // return model as array of obj
    function toArray() {
        const arr = []
        for (let i=0, len = count; i<len; ++i) {
            arr.push(get(i))
        }
        return arr
    }

    // sort model
    function sort(sortRole, compareFunc) {
        if (!Utils.isFunction(compareFunc))
            compareFunc = (role, item1, item2) => {
                if (item1[role] < item2[role])
                   return -1
                if (item1[role] > item2[role])
                   return 1
                else
                   return 0
            }

        sortRole = Utils.toRoleName(sortRole)
        let indexes = [...Array(count)].map( (v,i) => i )
        indexes.sort( (a, b) => compareFunc(sortRole, get(a), get(b)) )

        let sorted = 0
        while (sorted < indexes.length && sorted === indexes[sorted]) sorted++

        if (sorted === indexes.length) return

        for (let i = sorted; i < indexes.length; i++) {
           move(indexes[i], count - 1, 1)
           insert(indexes[i], { } )
        }
        remove(sorted, indexes.length - sorted)
    }
}

