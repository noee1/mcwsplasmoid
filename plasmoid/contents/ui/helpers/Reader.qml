import QtQuick
import 'utils.js' as Utils

QtObject {

    property string currentHost
    property string hostUrl

    readonly property var forEach: Array.prototype.forEach

    onCurrentHostChanged: hostUrl = "http://%1/MCWS/v1/".arg(currentHost)

    signal connectionError(string msg, string cmd)
    signal commandError(string msg, string cmd)

    // Run an mcws cmd, no return, no callback
    function exec(cmd) {
        __exec(cmd)
    }

    // Issue xhr mcws request, handle json/xml/text results
    function __exec(cmdstr, json) {

        return new Promise((resolve, reject) => {

            json = json !== undefined ? json : false

            const xhr = new XMLHttpRequest()

            xhr.onerror = () => {
                reject(cmdstr)
                connectionError("Unable to connect: ", cmdstr)
            }

            const isXml = () => {
                const h = xhr.getResponseHeader('Content-Type')
                return (h !== 'application/x-mediajukebox-mpl'
                        & h !== 'application/json')
            }

            xhr.onreadystatechange = () =>
            {
                if (xhr.readyState === XMLHttpRequest.DONE) {
                    if (xhr.status === 200) {
                        resolve(isXml()
                            ? xhr.responseXML.documentElement.childNodes
                            : xhr.response)
                    } else {
                        const err = (isXml() && xhr.responseXML
                               ? xhr.responseXML.documentElement.attributes[1].value
                               : 'Command error:')
                            + ' <status: %1:%2>'.arg(xhr.status).arg(xhr.statusText)
                        commandError(err, cmdstr)
                        reject(err + '\n' + cmdstr)
                    }
                }
            }

            cmdstr = hostUrl + cmdstr
            xhr.open("GET", cmdstr);
            xhr.responseType = json ? 'json' : 'text'
            xhr.send();

        }) // promise
    }

    // *************************************************
    // Get XML return from MCWS
    // *************************************************

    // Load a single object, callback(object)
    function loadObject(cmd) {

        return new Promise((resolve, reject) => {
           __exec(cmd).then(nodes =>
           {
               // XML nodes, builds obj as single object with props = nodes
               const obj = {}
               forEach.call(nodes, node =>
               {
                   if (node.nodeType === 1 && node.firstChild !== null) {
                       const role = Utils.toRoleName(node.attributes[0].value)
                       obj[role] = role.includes('name') || isNaN(node.firstChild.data)
                                   ? node.firstChild.data
                                   : +node.firstChild.data
                   }
               })
               resolve(obj)
               // MPL string (multiple Items/multiple Fields for each item) builds an array of item objs
//               } else if (typeof nodes === 'string') {
//                   console.log('MPL Command', cmd)
//                   const list = []
//                   __createObjectList(nodes, obj => { list.push(obj) })
//                   resolve(list)
           }, reject)

       })
    }

    /* / Load a model with Key/Value pairs
    function loadKVModel(cmd, model) {

        return new Promise((resolve, reject) => {
           __exec(cmd).then(nodes =>
           {
               // XML nodes, key = attr.name, value = node.value
               forEach.call(nodes, node =>
               {
                   if (node.nodeType === 1) {
                       model.append({ key: Utils.toRoleName(node.attributes[0].value)
                                    , value: isNaN(node.firstChild.data)
                                             ? node.firstChild.data
                                             : +node.firstChild.data })
                   }
               })
               resolve(model.count)

           }, reject)
       })
    }
    */

    /* / Load a model with MPL objs
    function loadModel(cmd, model) {

        return new Promise((resolve, reject) => {
           __exec(cmd).then(xmlStr =>
           {
               let defObj = {}
               if (model.count === 1) {
                   defObj = Object.assign({}, model.get(0))
                   model.remove(0)
               }

               __createObjectList(xmlStr, obj => model.append(Object.assign(defObj, obj)))

               resolve(model.count)

           }, reject)
       })
    }
    */

    /* / hard parse xml item/field for MPL-type lists
    function __createObjectList(xmlstr, callback) {
        const fldRegExp = /(?:< Name=")(.*?)(?:<\/>)/
        const items = xmlstr
//                .replace(/&quot;/g, '"')
//                .replace(/&#39;/g, "'")
//                .replace(/&lt;/g, '<')
//                .replace(/&gt;/g, '>')
            .replace(/&amp;/g, '&')
            .replace(/Field/g,'')
            .split('<Item>')

        // ignore first item, it's the MPL header info
        for (let i=1, len=items.length; i<len; ++i) {

            const fl = items[i].split('\r\n')
            let fields = {}
            fl.forEach(fldstr =>
            {
                const l = fldRegExp.exec(fldstr)
                if (l != null) {
                    const o = l.pop().split('">')
                    // Can't convert numbers, same field will vary (string/number)
                    fields[Utils.toRoleName(o[0])] = o[1]
                }
            })
            callback(fields)
        }
    }
*/

    // *************************************************
    // Get JSON return from MCWS
    // *************************************************

    // Get JSON array of objects
    function loadJSON(cmd) {

        return new Promise((resolve, reject) => {
           __exec(cmd, true).then(json =>
           {
              try {
                  const arr = []
                  json.forEach(item => {
                       const obj = {}
                       for (let p in item) {
                           obj[Utils.toRoleName(p)] = String(item[p])
                       }
                       arr.push(obj)
                  })
                  resolve(arr)
              } catch (err) {
                  reject(err)
              }

           }, reject)
        })
    }

    // Load MCWS JSON objects => model
    function loadModelJSON(cmd, model) {

        return new Promise((resolve, reject) => {
           // Look for/remove default obj which defines the model data structure
           let defObj = {}
           if (model.count === 1) {
               defObj = Object.assign({}, model.get(0))
               model.remove(0)
           }

           __exec(cmd, true).then(json =>
           {
               try {
                  json.forEach(item => {
                       const obj = Object.create(defObj)
                       for (let p in item)
                           obj[Utils.toRoleName(p)] = String(item[p])
                       model.append(obj)
                 })
                 resolve(model.count)
               }
               catch (err) {
                  reject(err)
               }
           }, reject)
        }) // promise
    }

}
