import QtQuick
import org.kde.kirigami as Kirigami

Kirigami.Separator {
    property string text: ''
    property bool isSection: true

    Kirigami.FormData.isSection: isSection
    Kirigami.FormData.label: text
}
