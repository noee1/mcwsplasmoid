import QtQuick

Timer {
    property var fn
    property var copyargs: []

    running: true
    onTriggered: {
        if (fn) {
            fn.apply(null, copyargs)
        } else {
            console.warn('OBJECT/FUNCTION NOT AVAILABLE', copyargs)
        }
        destroy()
    }
}
