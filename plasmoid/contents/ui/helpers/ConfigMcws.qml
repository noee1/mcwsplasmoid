import QtQuick
import QtQuick.Layouts
import QtQuick.Controls
import org.kde.kirigami as Kirigami

pragma ComponentBehavior: Bound

ColumnLayout {
    id: root

    property bool includeZones: true
    property bool allowMove: true

    property alias hostConfig: hostModel.configString
    property string defaultPort: '52199'

    McwsHostModel {
        id: hostModel

        autoLoad: false
        loadEnabledOnly: false

        onRowsMoved: save()
        onRowsRemoved: save()

        onLoadStart: hostModel.rowsInserted.disconnect(hostModel.save)
        onLoadFinish: hostModel.rowsInserted.connect(hostModel.save)

        property var alive: ({})

        function save() {
            Qt.callLater(() => {
                hostConfig = JSON.stringify(toArray())
            })
        }

        function setEnabled(index, val) {
            hostModel.setProperty(index, 'enabled', val)
            hostModel.save()
        }

        Component.onCompleted: Qt.callLater(load)
    }

    Reader { id: reader }

    function reset() {
        hostInfo.text = ''
        zoneList.clear()
    }

    function getZones(host: string) {
        zoneList.clear()
        reader.currentHost = host.indexOf(':') === -1
                ? host + ':' + defaultPort
                : host

        // check zone str for included zone
        const includeZone = (ndx,cfg) => {
              if (cfg) {
                  let arr = cfg.zones.split(',')
                  if (arr[0] === '*')
                    return true
                  else if (arr.includes(ndx.toString()))
                    return true
                  else
                    return false
              }
              // not config'd, default to true
              return true
          }

        // get host currently config'd host, if there
        let zoneCfg = hostModel.find(item => item.host === reader.currentHost)

        reader.loadObject('Playback/Zones')
        .then(zones => {
            for (let i=0, len=zones.numberzones; i<len; ++i) {
                  zoneList.append({key: zones['zoneid' + i].toString()
                                  , value: zones['zonename'+i]
                                  , include: includeZone(i,zoneCfg)})
                }
        }).catch(err => reset())
    }

    function getServerInfo(host: string) {
        hostInfo.text = 'searching...'
        reader.currentHost = host.indexOf(':') === -1
                ? host + ':' + defaultPort
                : host

        reader.loadObject('Alive')
        .then(obj => {
            hostModel.alive = obj
            hostInfo.text = '%1(%2): %3, v%4'
                              .arg(obj.friendlyname)
                              .arg(obj.accesskey)
                              .arg(obj.platform)
                              .arg(obj.programversion)
            getZones(reader.currentHost)

        }).catch(err => reset())
    }

    function saveZoneConfig() {
        if (includeZones) {
            // at least one zone must be selected
            if (!zoneList.contains(i => i.include)) {
                zoneMsg.visible = true
                return
            }

            let zonestr = ''
            let cnt = 0
            zoneList.forEach((item, ndx) => {
                             if (item.include) {
                                 zonestr += ',%1'.arg(ndx)
                                 ++cnt
                             }
                         })

            zonestr = cnt === zoneList.count
                        ? '*'
                        : zonestr.slice(1)

            // if host is config'd, update it, otherwise add it
            let ndx = hostModel.findIndex(item => item.host === reader.currentHost)
            if (ndx === -1) {
                hostModel.append({ host: reader.currentHost
                                , friendlyname: hostModel.alive.friendlyname
                                , accesskey: hostModel.alive.accesskey
                                , zones: zonestr
                                , enabled: true })
            } else {
                hostModel.setProperty(ndx, "zones", zonestr)
            }

            hostModel.save()
        }

    }

    Kirigami.InlineMessage {
        id: zoneMsg
        type: Kirigami.MessageType.Error
        showCloseButton: true
        Layout.fillWidth: true
        text: 'You must select at least one Zone'
    }

    // Lookup and Alive display
    RowLayout {
        Kirigami.SearchField {
            id: hostSearch
            placeholderText: 'Enter host name to search'
            onAccepted: {
                if (text.length === 0) {
                    root.reset()
                } else {
                    root.getServerInfo(hostSearch.text)
                }
            }
        }

        ToolButton {
            icon.name: 'list-add'
            visible: hostInfo.text.length > 0

            onClicked: root.saveZoneConfig()

            ToolTip {
                text: 'Add Host/Zone Config'
            }
        }

        Label {
            id: hostInfo
            elide: Text.ElideRight
            Layout.fillWidth: true
        }
    }

    component HostItemDelegate: Item {
        id: hid
        width: ListView.view.width
        implicitHeight: hostDelegate.implicitHeight

        required property int index
        required property string host
        required property string friendlyname
        required property string accesskey
        required property var model
        required property string zones

        ItemDelegate {
            id: hostDelegate

            width: hid.width
            text: hid.host + (root.includeZones ? ', Zones: [%1]'.arg(hid.zones) : '')
            down: false

            onClicked: {
                zoneMsg.visible = false
                ListView.currentIndex = hid.index
                hostInfo.text = ''
                getZones(hid.host)
            }

            contentItem: RowLayout {

                Kirigami.ListItemDragHandle {
                    listItem: hostDelegate
                    listView: lvHosts
                    onMoveRequested: (oldIndex, newIndex) => hostModel.move(oldIndex, newIndex, 1)
                }

                CheckBox {
                    checked: hid.model?.enabled ?? false
                    ToolTip {
                        text: 'Include this host'
                    }

                    onClicked: hostModel.setEnabled(hid.index, checked)
                }

                Kirigami.TitleSubtitle {
                    Layout.fillWidth: true
                    title: hostDelegate.text
                    subtitle: '%1, accesskey: %2'.arg(hid.friendlyname).arg(hid.accesskey)
                }

                ToolButton {
                    icon.name: 'delete'
                    ToolTip {
                        text: 'Remove'
                    }

                    onClicked: {
                        hostModel.remove(hid.index)
                        root.reset()
                    }
                }
            }
        }
    }
    // Config setup and update
    ListView {
        id: lvHosts
        model: hostModel
        Layout.fillHeight: true
        Layout.fillWidth: true
        Layout.minimumHeight: Math.round(root.height * .35)
        clip: true
        spacing: 0

        moveDisplaced: Transition {
            YAnimator {
                duration: Kirigami.Units.longDuration
                easing.type: Easing.InOutQuad
            }
        }

        delegate: HostItemDelegate {}
    }

    GroupSeparator {
        text: root.includeZones ? 'Select Zones to Include' : 'Playback Zones'
        opacity: zoneList.count > 0
    }

    ListView {
        id: zoneView
        model: BaseListModel { id: zoneList }
        Layout.minimumHeight: Math.round(root.height * .35)
        Layout.fillWidth: true
        Layout.fillHeight: true
        clip: true
        delegate: CheckDelegate {

            required property string key
            required property string value
            required property bool include
            required property int index

            width: Math.round(zoneView.width*.5)
            text: '%1 (id: %2)'.arg(value).arg(key)
            checkable: root.includeZones
            checked: include
            onClicked: {
                zoneMsg.visible = false
                zoneList.setProperty(index, "include", checked)
                Qt.callLater(root.saveZoneConfig)
            }
        }
    }

}
