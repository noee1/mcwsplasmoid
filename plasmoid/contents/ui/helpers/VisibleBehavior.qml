import QtQuick

FadeBehavior {
    exitAnimation.duration: targetValue ? 0 : fadeDuration
    enterAnimation.duration: targetValue ? fadeDuration : 0
}
