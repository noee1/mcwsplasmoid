import QtQuick

QtObject {
    enum PlayerState {
        Stopped = 0,
        Paused,
        Playing,
        Aborting,
        Buffering
    }
}
