﻿import QtQuick
import QtQuick.Layouts
import org.kde.plasma.plasmoid
import org.kde.plasma.core as PlasmaCore
import org.kde.kirigami as Kirigami

import 'helpers'
import 'theme'

pragma ComponentBehavior: Bound

PlasmoidItem {
    id: plasmoidRoot

    property bool vertical:         Plasmoid.formFactor === PlasmaCore.Types.Vertical
    property bool panelZoneView:    Plasmoid.configuration.advancedTrayView & !vertical

    property bool abbrevTrackView:  Plasmoid.configuration.abbrevTrackView
    property bool autoShuffle:      Plasmoid.configuration.autoShuffle
    property bool autoConnect:      Plasmoid.configuration.autoConnect
    property int  thumbSize:        Plasmoid.configuration.thumbSize

    // hold ref to dynamic ss/splash
    property var splasher

    /*  Background color theming  */
    property ThemeSetup themeSetup: ThemeSetup {
        enabled: mcws.isConnected && Plasmoid.configuration.useTheme
        config:  Plasmoid.configuration.themes
        dark:    Plasmoid.configuration.themeDark
        name:    Plasmoid.configuration.themeName
    }

    // Use these signals to communicate to/from compact view and full view
    signal zoneSelected(int zonendx)
    signal tryConnection()
    signal hostModelChanged(int index)
    signal toogleScreenSaver()

    Plasmoid.icon: "multimedia-player"

    // Compact components
    Component {
        id: advComp

        CompactView {
            property int lastZone: -1

            onZoneHovered: (tt) => {
                toolTipMainText = '%0 on [%1]: %2 (%3) Vol: %4\n'.arg(tt.zone)
                               .arg(mcws.serverInfo.friendlyname)
                               .arg(tt.status)
                               .arg(tt.pos)
                               .arg(tt.vol)
                toolTipSubText = tt.next
            }

            onZoneClicked: zonendx => {
                // if connected, keep the popup open
                // when clicked a different zone
                if (mcws.isConnected) {
                    // compact view uses sort/filter model, convert index
                    zonendx = mcws.zonesWithPlaylist.mapRowToSource(zonendx)
                    /*emit*/ plasmoidRoot.zoneSelected(zonendx)
                    if (!plasmoidRoot.expanded) {
                        lastZone = zonendx
                        plasmoidRoot.expanded = true
                    }
                    else {
                        if (lastZone === zonendx)
                            plasmoidRoot.expanded = false
                        else
                            lastZone = zonendx
                    }
                    return
                }

                // not connected
                if (!plasmoidRoot.expanded) {
                    /*emit*/ tryConnection()
                    plasmoidRoot.expanded = true
                }
                else if (plasmoidRoot.hideOnWindowDeactivate)
                    plasmoidRoot.expanded = false
            }
        }
    }

    Component {
        id: iconComp

        // Copied from P-D/DefaultCompactRepresentation
        Kirigami.Icon {
            Component.onCompleted: plasmoidRoot.toolTipSubText = 'Click to Connect'

            Layout.minimumWidth: {
                switch (Plasmoid.formFactor) {
                case PlasmaCore.Types.Vertical:
                    return 0;
                case PlasmaCore.Types.Horizontal:
                    return height;
                default:
                    return Kirigami.Units.gridUnit * 3;
                }
            }

            Layout.minimumHeight: {
                switch (Plasmoid.formFactor) {
                case PlasmaCore.Types.Vertical:
                    return width;
                case PlasmaCore.Types.Horizontal:
                    return 0;
                default:
                    return Kirigami.Units.gridUnit * 3;
                }
            }

            source: Plasmoid.icon || "plasma"
            active: mouseArea.containsMouse

            activeFocusOnTab: true

            Keys.onPressed: event => {
                switch (event.key) {
                case Qt.Key_Space:
                case Qt.Key_Enter:
                case Qt.Key_Return:
                case Qt.Key_Select:
                    Plasmoid.activated();
                    event.accepted = true; // BUG 481393: Prevent system tray from receiving the event
                    break;
                }
            }

            Accessible.name: Plasmoid.title
            Accessible.description: plasmoidRoot.toolTipSubText ?? ""
            Accessible.role: Accessible.Button

            MouseArea {
                id: mouseArea

                property bool wasExpanded: false

                anchors.fill: parent
                hoverEnabled: true
                onPressed: wasExpanded = plasmoidRoot.expanded
                onClicked: {
                    if (plasmoidRoot.expanded & !plasmoidRoot.hideOnWindowDeactivate)
                        return
                    plasmoidRoot.expanded = !wasExpanded
                }
            }
        }
    }

    switchWidth:  Kirigami.Units.gridUnit * 25
    switchHeight: Kirigami.Units.gridUnit * 25

    preferredRepresentation: compactRepresentation

    compactRepresentation: Loader {

        Layout.preferredWidth: {
            if (mcws.isConnected) {
                if (plasmoidRoot.panelZoneView) {
                    if (Plasmoid.configuration.useZoneCount)
                        return Math.round(plasmoidRoot.height
                                * Math.max(2, mcws.zonesWithPlaylist.count)
                                * Math.round(Kirigami.Units.smallSpacing*1.25))
                    else
                        return Math.round(Kirigami.Units.gridUnit
                                * Plasmoid.configuration.trayViewSize)
                }
            }
            // icon
            return -1

        }

        sourceComponent: {
            if (!mcws.isConnected || (!plasmoidRoot.panelZoneView | mcws.zonesWithPlaylist.count === 0))
                return iconComp

            return advComp
        }
    }

    fullRepresentation: FullView {
        readonly property QtObject appletInterface: plasmoidRoot

        Layout.preferredWidth: Kirigami.Units.gridUnit * 50
        Layout.preferredHeight: Kirigami.Units.gridUnit * 25

        Layout.minimumWidth: implicitWidth
        Layout.maximumWidth: Kirigami.Units.gridUnit * 80
        Layout.minimumHeight: implicitHeight
        Layout.maximumHeight: Kirigami.Units.gridUnit * 60
    }

    SingleShot { id: event }

    Connections {
        target: Plasmoid.configuration

        function onShowTrackSplashChanged() {
            if (Plasmoid.configuration.showTrackSplash) {
                splasher = tsComp.createObject(plasmoidRoot, {showTrackSplash: true})
                logger.log('TrackSplash Enabled')
            }
            else {
                splasher.destroy()
                splasher = null
                logger.log('TrackSplash Disabled')
            }
        }

        function onUseZoneCountChanged() { mcws.reset() }

        function onTrayViewSizeChanged() {
            if (!Plasmoid.configuration.useZoneCount)
                mcws.reset()
        }

        function onAllowDebugChanged() {
            if (Plasmoid.configuration.allowDebug)
                logger.init()
            else
                logger.close()
        }
    }

    McwsHostModel {
        id: hostModel

        configString: Plasmoid.configuration.hostConfig

        onLoadError: msg => logger.log('HOSTCFG LOAD ERR', msg)

        onLoadFinish: count => {
            // model with no rows means config is not set up
            if (count === 0) {
                mcws.closeConnection()
            } else if (plasmoidRoot.autoConnect || mcws.isConnected) {
                // If the connected host is not in the list, reset connection to first in list
                // Also, this is essentially the auto-connect at plasmoid load (see Component.completed)
                // because at load time, mcws.host is null (mcws is not connected)
                let ndx = hostModel.findIndex(item => item.host === mcws.host)
                if (ndx === -1) {  // not in model, so set to first
                    mcws.hostConfig = Object.assign({}, hostModel.get(0))
                    ndx = 0
                } else { // current connection is in model
                    // check if zones changed, reset connection if true
                    if (hostModel.get(ndx).zones !== mcws.hostConfig.zones) {
                        mcws.hostConfig = Object.assign({}, hostModel.get(ndx))
                        mcws.reset()
                    }
                }
                hostModelChanged(ndx)
            }
        }

    }

    McwsConnection {
        id: mcws

        videoFullScreen:   Plasmoid.configuration.forceDisplayView
        highQualityThumbs: Plasmoid.configuration.highQualityThumbs
        pollerInterval:    Plasmoid.configuration.updateInterval/100 * 1000
        defaultFields:     Plasmoid.configuration.defaultFields
    }

    // Screen saver and track splasher
    // screensaver options are per-plasmoid-session
    // track splash options are in config/playback
    Component {
        id: tsComp

        SplashBase {
            fullScreenSplash : Plasmoid.configuration.fullscreenTrackSplash
            animateSplash    : Plasmoid.configuration.animateTrackSplash
            splashDuration   : Plasmoid.configuration.splashDuration * 10

            Connections {
                target: mcws

                // Only splash the track when it changes, when the
                function onTrackKeyChanged(zonendx, filekey) {
                    // need to wait for state here, buffering etc.
                    event.queueCall(2000, () => {
                        let zone = mcws.zoneModel.get(zonendx)
                        // Only show playing, legit tracks
                        if (zone.state !== PlayerState.Playing || filekey === '-1')
                            return

                        init()
                        panelModel.append(newItem(zone, zonendx, filekey,
                                         { splashmode: true
                                         , screensaver: false
                                         , animate: animateSplash
                                         , fullscreen: fullScreenSplash
                                         , transparent: false
                                         , duration: splashDuration
                                         }))
                    })
                }
            }

        }
    }

    Component {
        id: ssComp

        SplashBase {
            useCoverArt    : Plasmoid.configuration.useCoverArtBackground
            animateSS      : Plasmoid.configuration.animatePanels
            transparentSS  : Plasmoid.configuration.transparentPanels
            useMultiScreen : Plasmoid.configuration.useMultiScreen

            Connections {
                target: mcws

                // Only splash the track when it changes, when the
                // screenSaver mode is not enabled and when it's playing
                function onTrackKeyChanged(zonendx, filekey) {
                    event.queueCall(1000, addItem, zonendx, filekey,
                                    { animate: animateSS
                                        , fullscreen: false
                                        , screensaver: true
                                        , splashmode: false
                                        , transparent: transparentSS
                                      })
                }

                function onConnectionStart(host) {
                    screenSaverMode = false
                }

                function onConnectionStopped() {
                    screenSaverMode = false
                }
            }

            onDone: {
                splasher.destroy()
                splasher = null
                logger.log('SS DONE')
            }
        }
    }

    // Auto-connect Watcher
    Timer {
        interval: 10000; repeat: true

        running: !mcws.isConnected && plasmoidRoot.autoConnect

        onRunningChanged: {
            logger.log(running ? 'WATCHER RUNNING' : 'WATCHER STOPPED')
        }

        onTriggered: {
            if (hostModel.count > 0) {
                logger.log('WATCHER TRYING CONNECTION')
                /*emit*/ plasmoidRoot.tryConnection()
            } else {
                logger.log('WATCHER LOADING HOSTMODEL')
                hostModel.load()
            }
        }

    }

    // Logger for "simple" debug items
    Connections {
        target: mcws
        enabled: Plasmoid.configuration.allowDebug & logger.enabled

        function onDebugLogger(title, msg, obj) {
            logger.log(title, msg, obj)
        }

        function onConnectionStart(host) {
            logger.warn('ConnectionStart', host, mcws.hostConfig)
        }

        function onConnectionStopped() {
            logger.warn('ConnectionStopped')
        }

        function onConnectionReady(host, zonendx) {
            logger.warn('ConnectionReady', '(%1)'.arg(zonendx) + host, mcws.hostConfig)
        }

        function onConnectionError(msg, cmd) {
            logger.warn('ConnectionError', msg, cmd)
        }

        function onCommandError(msg, cmd) {
            logger.warn('CommandError:', msg, cmd)
        }

        function onTrackKeyChanged(zonendx, filekey) {
            logger.log(mcws.zoneModel.get(zonendx).zonename + ':  TrackKeyChanged'
                       , filekey.toString())
        }

        function onPnPositionChanged(zonendx, pos) {
            logger.log(mcws.zoneModel.get(zonendx).zonename + ':  PnPositionChanged'
                       , pos.toString())
        }

        function onPnChangeCtrChanged(zonendx, ctr) {
            logger.log(mcws.zoneModel.get(zonendx).zonename + ':  PnChangeCtrChanged'
                       , ctr.toString())
        }

        function onPnStateChanged(zonendx, playerState) {
            logger.log(mcws.zoneModel.get(zonendx).zonename + ':  PnStateChanged'
                       , 'State: ' + playerState.toString())
        }
    }
    Logger {
        id: logger
        winTitle: 'MCWS Logger'
    }

    Plasmoid.contextualActions: [
        PlasmaCore.Action {
            text: "Logger Window"
            icon.name: "debug-step-into"
            visible: Plasmoid.configuration.allowDebug
            onTriggered: logger.init()
        }
        , PlasmaCore.Action {
            isSeparator: true
        }
        , PlasmaCore.Action {
            id: ssAction
            text: {
                if (mcws.isConnected && (!splasher || splasher.screenSaverMode))
                    return (splasher && splasher.screenSaverMode ? 'Stop' : 'Start') + ' Screensaver'
                else
                    return ''
            }
            icon.name: 'preferences-desktop-screensaver-symbolic'
            visible: mcws.isConnected && (!splasher || splasher.screenSaverMode)
            onTriggered: event.queueCall(plasmoidRoot.toogleScreenSaver)
        }
        , PlasmaCore.Action {
            text: "Refresh View"
            icon.name: "view-refresh"
            visible: mcws.isConnected
            onTriggered: mcws.reset()
        }
        , PlasmaCore.Action {
            text: "Close Connection"
            icon.name: "network-disconnect-symbolic"
            visible: mcws.isConnected
            onTriggered: {
                mcws.closeConnection()
                plasmoidRoot.expanded = false
            }
        }
        , PlasmaCore.Action {
            isSeparator: true
        }

    ]

    PlasmaCore.Action {
        id: configAction
        text: 'Configure…'
        icon.name: 'configure'
        onTriggered: Plasmoid.containment.configureRequested(Plasmoid)
    }

    Component.onCompleted: {
        if (Plasmoid.configuration.allowDebug) {
            logger.init()
        }

        Plasmoid.setInternalAction('configure', configAction)

        Plasmoid.backgroundHints = PlasmaCore.Types.DefaultBackground
                        | PlasmaCore.Types.ConfigurableBackground
    }
}
