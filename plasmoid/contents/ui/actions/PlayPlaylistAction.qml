import QtQuick

BaseAction {
    aText: 'Playlist: "%1"'.arg(playlistView.currentName)
    method: 'play'
    enabled: playlistView.viewer.count > 0
    onTriggered: {
        event.queueCall(() => {
            zoneView.currentPlayer.playPlaylist(playlistView.currentID, shuffle)
            mainView.currentIndex = 1
        })
    }
}
