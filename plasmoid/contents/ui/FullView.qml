import QtQuick
import QtQuick.Layouts
import QtQuick.Controls

import org.kde.plasma.plasmoid
import org.kde.kirigami as Kirigami
import org.kde.plasma.extras as PE
import org.kde.plasma.components as PComp

import 'helpers'
import 'models'
import 'controls'
import 'actions'
import 'theme'

pragma ComponentBehavior: Bound

PE.Representation {
    id: root

    collapseMarginsHint: true

    // default to zoneview/trackview at startup
    Component.onCompleted: mainView.currentIndex = 1

    Connections {
        target: mcws

        // If the playing position changes for the current zone
        function onPnPositionChanged(zonendx, pos) {
            if (mcws.isConnected
                    && zoneView.isCurrent(zonendx)) {
                event.queueCall(500, trackView.highlightPlayingTrack)
            }
        }

        // Initialize some vars when a connection starts
        function onConnectionStart(host) {
            zoneView.viewer.model = ''
            trackView.viewer.model = ''
            searchButton.checked = false
            trackView.mcwsQuery = ''
            searcher.init()
        }

        // Connection closed or a host reset to null
        function onConnectionStopped() {
            zoneView.viewer.model = ''
            trackView.viewer.model = ''
            mainView.currentIndex = 1
        }

        // Set current zone view when connection signals ready
        function onConnectionReady(host, zonendx) {
            zoneView.viewer.model = mcws.zoneModel

            // For each zone tracklist, catch sort reset
            mcws.zoneModel.forEach(zone => {
                zone.trackList.sortReset.connect(trackView.highlightPlayingTrack)
            })

            mainView.currentIndex = 1
            event.queueCall(500, trackView.highlightPlayingTrack)
        }

        // On error, reset view to the zoneview page
        function onCommandError(msg, cmd) {
            if (cmd.includes(mcws.host)) {
                mainView.currentIndex = 1
            }
        }
    }

    Connections {
        id: plasmoidConn
        target: plasmoidRoot

        function onHostModelChanged(index) {
            hostSelector.currentIndex = index
        }

        // When a zone is clicked in compact view
        function onZoneSelected(zonendx) {
            zoneView.set(zonendx)
        }

        // Try a connection based on currently selected host
        function onTryConnection() {
            if (hostModel.count > 0 && hostSelector.currentIndex !== -1)
                mcws.hostConfig = Object.assign({}, hostModel.get(hostSelector.currentIndex))
        }

        function onExpandedChanged() {
            if (plasmoidRoot.expanded && !mcws.isConnected)
                event.queueCall(onTryConnection)
        }

        function onToogleScreenSaver() {
            if (splasher && splasher.screenSaverMode) {
                splasher.screenSaverMode = false
            } else {
                splasher = ssComp.createObject(plasmoidRoot, {screenSaverMode: true})
            }
        }

    }

    Connections {
        target: mcws.playlists.trackModel
        enabled: mcws.isConnected

        // Handle playlist track searching/display
        function onSearchBegin() {
            busyInd.visible = true
        }

        function onSearchDone() {
            busyInd.visible = false
            trackView.highlightPlayingTrack()
            sorter.target = mcws.playlists.trackModel
        }

        function onSortReset() {
            trackView.highlightPlayingTrack()
        }
    }

    Connections {
        target: mcws.quickSearch
        enabled: mcws.isConnected

        function onResultsReady(querytype, count) {
            lookupPage.resetUi(querytype)
        }
    }

    // Library searcher
    Searcher {
        id: searcher
        comms: mcws.comms
        autoShuffle: Plasmoid.configuration.shuffleSearch
        mcwsFields: mcws.mcwsFieldsModel

        onSearchBegin: busyInd.visible = true

        onSearchDone: {
            busyInd.visible = false
            trackView.highlightPlayingTrack()
            sorter.target = searcher
        }

        onSortReset: trackView.highlightPlayingTrack()

        onDebugLogger: (title, msg, obj) => logger.log(title, msg, obj)
    }

    PComp.SwipeView {
        id: mainView
        anchors.fill: parent
        interactive: mcws.isConnected
        spacing: Kirigami.Units.smallSpacing

        onCurrentIndexChanged: mainView.itemAt(currentIndex).viewEntered()

        // Playlist View
        ViewerPage {
            id: playlistView

            property string currentID
            property string currentName

            // Lazy load
            onViewEntered: {
                if (viewer.count === 0) {
                    event.queueCall(() => {
                        mcws.playlists.load()
                        viewer.model = mcws.playlists.items
                    })
                }
                listPositioner.list = viewer
            }

            background: BackgroundTheme {
                theme: plasmoidRoot.themeSetup
                ignoreDefault: true
                mainImage: zoneView.currentItem?.bkImage ?? null
            }

            header: RowLayout {

                Kirigami.Heading {
                    Layout.fillWidth: true
                    level: 2
                    text: "Playlists [%1]".arg((zoneView.currentZone
                                         ? zoneView.currentZone.zonename
                                         : ''))
                }

                Kirigami.SearchField {
                    placeholderText: 'Filter'
                    onTextChanged: mcws.playlists.filterString = text
                }

                Repeater {
                    id: plActions
                    model: mcws.playlists.searchActions
                    delegate: PComp.ToolButton {
                        required property var modelData
                        checkable: true
                        action: modelData
                        autoExclusive: true
                    }
                }
            }

            viewer.reuseItems: true
            viewer.useHighlight: false
            viewer.delegate: PComp.ItemDelegate {
                width: ListView.view.width

                required property string name
                required property string path
                required property string id
                required property string type

                contentItem: RowLayout {
                    Kirigami.IconTitleSubtitle {
                        icon.name: mcws.playlists.icon(type)
                        title: name
                        subtitle: path
                        Layout.fillWidth: true
                    }

                    Loader {
                        id: plControls
                        active: false

                        sourceComponent: RowLayout {
                            Layout.alignment: Qt.AlignRight
                            visible: false
                            VisibleBehavior on visible {}

                            Component.onCompleted: {
                                visible = true
                                // Set current
                                playlistView.currentID = id
                                playlistView.currentName = name
                            }

                            PlayButton {
                                action: PlayPlaylistAction {
                                    shuffle: autoShuffle
                                }
                            }
                            AppendButton {
                                action: AddPlaylistAction {
                                    shuffle: autoShuffle
                                }
                            }
                            ShowButton {
                                onClicked: {
                                    trackView.setPlaylistMode()
                                    mcws.playlists.loadTracks(id)
                                }
                            }
                        }
                    }
                }

                onHoveredChanged: {
                    if (!hovered) {
                        plControls.active = false
                        cTimer.stop()
                    } else {
                        cTimer.start()
                    }
                }

                Timer {
                    id: cTimer
                    interval: Kirigami.Units.shortDuration
                    onTriggered: plControls.active = hovered
                }


            }

        }

        // Zone/Tracks Viewers, 2 columns in the split
        SplitView {
            signal viewEntered()
            onViewEntered: listPositioner.list = trackView.viewer

            handle: Rectangle {
                implicitWidth: Kirigami.Units.smallSpacing
                implicitHeight: Kirigami.Units.smallSpacing
                color: SplitHandle.pressed
                       ? "#81e889"
                       : Kirigami.Theme.disabledTextColor
            }

            // Zone Viewer
            ViewerPage {
                id: zoneView

                SplitView.preferredWidth: Math.round(mainView.width/2)
                SplitView.minimumWidth: Math.round(mainView.width/4)

                header: PComp.ToolBar {
                    RowLayout {
                        anchors.fill: parent

                        PComp.ToolButton {
                            icon.name: 'open-menu-symbolic'

                            onClicked: {
                                if (!globalMenu.active)
                                    globalMenu.active = true
                                else
                                    globalMenu.item.popup()
                            }

                            PComp.ToolTip {
                                text: 'General Options'
                            }
                        }

                        Item { Layout.fillWidth: true }

                        Kirigami.Heading {
                            level: 2
                            text: i18n("Playback Zones on: ")
                        }

                        PComp.ComboBox {
                            id: hostSelector
                            // Layout.fillWidth: true
                            model: hostModel
                            textRole: 'friendlyname'
                            onActivated: {
                                mcws.hostConfig = Object.assign({}, model.get(currentIndex))
                            }
                        }

                    }
                }

                // use var instead of alias, they are dyn model items
                readonly property var currentZone: zoneView.viewer.modelItem
                readonly property var currentPlayer: zoneView.viewer.modelItem?.player ?? null

                readonly property alias count: zoneView.viewer.count
                property alias currentItem: zoneView.viewer.currentItem
                property alias currentIndex: zoneView.viewer.currentIndex

                viewer.model: mcws.zoneModel
                viewer.useHighlight: false
                viewer.delegate: ZoneDelegate {}

                onCurrentIndexChanged: {
                    if (currentIndex === -1)
                        return

                    event.queueCall(100, () => {
                        let z = zoneView.viewer.model.get(currentIndex)
                        if (!trackView.searchMode)
                            trackView.reset(z)

                        logger.log('GUI:ZoneChanged'
                                   , '%3, index: %1, TrackList Cnt: %2'
                                       .arg(currentIndex)
                                       .arg(trackView.count)
                                       .arg(z.zonename))
                    })
                }

                function set(zonendx) {
                    // Form factor constraints, vertical do nothing
                    if (vertical) {
                        if (currentIndex === -1)
                            currentIndex = mcws.getPlayingZoneIndex()
                    }
                    // Inside a panel...
                    else {
                        // no zone change, do nothing
                        if (isCurrent(zonendx))
                            return

                        currentIndex = zonendx !== -1
                                ? zonendx
                                : mcws.getPlayingZoneIndex()
                    }
                }

                function isCurrent(zonendx) {
                    return zonendx === currentIndex
                }

                Loader {
                    active: zoneView.count === 0
                    anchors.centerIn: parent
                    width: parent.width - (Kirigami.Units.largeSpacing * 4)

                    sourceComponent: Kirigami.PlaceholderMessage {
                        icon.name: 'dialog-question'
                        text: mcws.isConnected
                              ? '%1 (v%2)'
                                .arg(mcws.serverInfo.programname)
                                .arg(mcws.serverInfo.programversion)
                              : hostSelector.count > 0
                                ? 'Media Server "%1" is not available'.arg(hostSelector.currentText)
                                : 'Invalid MCWS host configuration?'

                        helpfulAction: Action {
                            icon.name: "configure"
                            text: "Check MCWS Config…"
                            onTriggered: configAction.triggered()
                        }
                    }
                }
            }

            // Trackview
            ViewerPage {
                id: trackView

                SplitView.minimumWidth: Math.round(mainView.width/4)

                header: PComp.ToolBar {
                    RowLayout {
                        anchors.fill: parent
                        opacity: mcws.isConnected ? 1 : 0

                        // Enter/exit search mode
                        ShowButton {
                            id: searchButton
                            checkable: true
                            icon.name: checked ? 'edit-undo' : 'search'
                            tipText: checked
                                    ? 'Back to Playing Now'
                                    : 'Search Library'

                            onClicked: {
                                if (!checked) {
                                    trackView.reset()
                                }
                                else {
                                    sorter.target = searcher
                                    trackView.viewer.model = searcher.items
                                    trackView.mcwsQuery = searcher.constraintString
                                }
                            }
                        }

                        // Sort the current tracklist
                        // Search list, playlist or playing now
                        SortButton { id: sorter }

                        // play/add Playlist
                        PlayButton {
                            action: PlayPlaylistAction {
                                shuffle: autoShuffle
                            }
                            visible: trackView.showingPlaylist
                        }
                        AppendButton {
                            action: AddPlaylistAction {
                                shuffle: autoShuffle
                            }
                            visible: trackView.showingPlaylist
                        }

                        // play doctor
                        Kirigami.SearchField {
                            id: quickPlay
                            visible: !searchButton.checked
                            placeholderText: "Quick Play"
                            delaySearch: true
                            Layout.fillWidth: true

                            onVisibleChanged: {
                                if (visible)
                                    forceActiveFocus()
                            }

                            onAccepted: {
                                if (text.length === 0) {
                                    return
                                }

                                zoneView.currentZone.player.playDoctor(text)
                            }
                        }

                        // Filter current playlist
                        Kirigami.SearchField {
                            id: plFilter
                            Layout.fillWidth: true
                            placeholderText: 'Filter'
                            visible: !searchButton.checked

                            onTextChanged:
                                zoneView.currentZone.trackList.items.filterString = text
                        }

                        // Playlist label
                        Kirigami.Heading {
                            Layout.fillWidth: true
                            horizontalAlignment: Qt.AlignRight
                            level: 5
                            visible: trackView.showingPlaylist
                            text: '"%1"'.arg(playlistView.currentName)
                        }

                        // Search text entry
                        Kirigami.SearchField {
                            id: searchField
                            font.pointSize: Kirigami.Theme.defaultFont.pointSize-1
                            Layout.fillWidth: true
                            placeholderText: 'Search Library'
                            visible: !trackView.showingPlaylist & searchButton.checked

                            onVisibleChanged: {
                                if (visible)
                                    forceActiveFocus()
                            }

                            autoAccept: false
                            onAccepted: {
                                if (searchField.length === 0)
                                    searcher.clear()
                                else
                                    // One char is a "starts with" search
                                    trackView.search(searchField.text.length === 1
                                                         ? '[%1"'.arg(searchField.text) // startsWith search
                                                         : '"%1"'.arg(searchField.text) // Like search
                                                     , false)
                            }
                        }

                        // Play/add the current list
                        PlayButton {
                            action: PlaySearchListAction {
                                text: ''
                                shuffle: autoShuffle
                            }
                            visible: searchButton.checked & !trackView.showingPlaylist
                        }
                        AppendButton {
                            action: AddSearchListAction {
                                text: ''
                                shuffle: autoShuffle
                            }
                            visible: searchButton.checked & !trackView.showingPlaylist
                        }

                    }
                }

                property alias currentTrack: trackView.viewer.modelItem
                property alias count: trackView.viewer.count
                property alias currentItem: trackView.viewer.currentItem
                property alias currentIndex: trackView.viewer.currentIndex

                property string mcwsQuery: ''
                property bool searchMode: mcwsQuery.length > 0
                property bool showingPlaylist: mcwsQuery === 'playlist'
                property bool filtered: plFilter.length > 0
                property bool playDoctor: quickPlay.length > 0

                viewer.useHighlight: false
                viewer.delegate: TrackDelegate {}

                function highlightPlayingTrack() {
                    if (trackView.count === 0) return
                    if (searchMode
                            & !Plasmoid.configuration.showPlayingTrack)
                        return

                    const ndx = viewer.model.findIndex(item => +item.key === +zoneView.currentZone.filekey)
                    currentIndex = ndx === -1 ? 0 : ndx
                    viewer.positionViewAtIndex(currentIndex, ListView.Beginning)
                    event.queueCall(500, currentItem.animateTrack)
                }

                // contraints can be a string or obj. obj should be of form:
                // { artist: value, album: value, genre: value, etc.... }
                // if str is passed, then default search fields are used
                function search(constraints, andTogether) {
                    viewer.model = searcher.items
                    searcher.logicalJoin = (andTogether === true || andTogether === undefined
                                            ? 'and' : 'or')
                    searcher.search(constraints)

                    searchField.text = (typeof constraints === 'object')
                        ? constraints[Object.keys(constraints)[0]].replace(/(\[|\]|\")/g, '')
                        : constraints.replace(/(\[|\]|\")/g, '')

                    mcwsQuery = searcher.constraintString
                    searchButton.checked = true
                    mainView.currentIndex = 1
                }

                // Puts the view in search mode,
                // sets the view model to the playlist tracks
                // and loads the model
                function setPlaylistMode() {
                    mainView.currentIndex = 1
                    mcwsQuery = 'playlist'
                    searchButton.checked = true
                    viewer.model = mcws.playlists.trackModel.items
                }

                // Set the viewer to the zone playing now
                // and exit search mode
                function reset(zone) {
                    if (zone === undefined || zone === null)
                        zone = zoneView.currentZone

                    mcwsQuery = ''
                    searchButton.checked = false
                    viewer.model = zone.trackList.items
                    sorter.target = zone.trackList
                    event.queueCall(highlightPlayingTrack)
                }

                Loader {
                    active: trackView.count === 0
                             && !searchButton.checked
                             && !trackView.filtered
                             && !trackView.playDoctor
                    anchors.centerIn: parent
                    width: parent.width - (Kirigami.Units.largeSpacing * 4)

                    sourceComponent: Kirigami.PlaceholderMessage {
                        icon.name: mcws.isConnected
                                  ? 'none' //'source-smart-playlist'
                                  : 'network-disconnected'

                        text: mcws.isConnected && zoneView.currentZone
                              ? (searchButton.checked
                                      ? "Searching…Please Wait…"
                                      : "Current Playlist on [%1] is empty"
                                        .arg(zoneView.currentZone.zonename))
                              : ''
                        explanation: mcws.isConnected && zoneView.currentZone
                                ? 'Try searching for tracks or choose a Playlist'
                                : ''

                        helpfulAction: Action {
                            icon.name: "source-smart-playlist-symbolic"
                            text: "Show Playlists"
                            enabled: mcws.isConnected && zoneView.currentZone
                            onTriggered: {
                                mainView.currentIndex = 0
                            }
                        }
                    }
                }

                BusyIndicator {
                    id: busyInd
                    visible: false
                    anchors.centerIn: parent
                    implicitWidth: parent.width/4
                    implicitHeight: implicitWidth
                }

            }
        }

        // Quick search lookups
        ViewerPage {
            id: lookupPage

            function resetUi(type) {
                if (type === LookupValues.QueryType.ByField) {
                    sb.scrollCurrent()
                    lSrch.clear()
                    sb.visible = true
                } else {
                    sb.reset()
                    sb.visible = false
                }
            }

            onViewEntered: {
                if (viewer.count === 0) {
                    // default to first search option
                    lookupButtons.itemAt(0).checked = true
                    event.queueCall(lookupButtons.itemAt(0).clicked)
                }
                listPositioner.list = viewer
            }

            background: BackgroundTheme {
                theme: plasmoidRoot.themeSetup
                ignoreDefault: true
                mainImage: zoneView.currentItem?.bkImage ?? null
            }

            header: ColumnLayout {
                spacing: Kirigami.Units.smallSpacing
                RowLayout {
                    Kirigami.Heading {
                        text: 'Library Search'
                        level: 2
                        Layout.fillWidth: true
                    }

                    Repeater {
                        id: lookupButtons
                        model: mcws.mcwsFieldsModel
                        delegate: PComp.ToolButton {

                            required property string field
                            required property bool searchable

                            checkable: true
                            text: field
                            visible: searchable
                            autoExclusive: true
                            checked: text === mcws.quickSearch.queryField
                            onClicked: mcws.quickSearch.queryField = text
                            icon.name: mcws.quickSearch.getIcon(text)
                        }
                    }

                    Kirigami.SearchField {
                        id: lSrch
                        placeholderText: "Search"
                        delaySearch: true
                        onAccepted: {
                            if (text.length === 0)
                                mcws.quickSearch.clear()
                            else
                                mcws.quickSearch.queryFilter = text
                        }
                    }

                    PComp.ToolButton {
                        id: showBtn
                        icon.name: checked ? 'music-note-16th' : 'media-optical-mixed-cd'
                        checkable: true
                        checked: mcws.quickSearch.mediaType === 'audio'
                        autoExclusive: false
                        onCheckedChanged: mcws.quickSearch.mediaType = checked
                                          ? 'audio'
                                          : ''

                        ToolTip {
                            text: showBtn.checked
                                  ? 'Showing Audio Only'
                                  : 'Showing All Media'
                        }
                    }

                }

                SearchBar {
                    id: sb
                    list: lookupPage.viewer
                    role: "value"
                    Layout.alignment: Qt.AlignCenter
                }
            }

            viewer.model: mcws.quickSearch.items
            viewer.useHighlight: false

            viewer.delegate: PComp.ItemDelegate {
                id: qsDel
                width: ListView.view.width

                required property string value
                required property string field
                required property string iconName

                contentItem: RowLayout {
                    Kirigami.IconTitleSubtitle {
                        title: qsDel.value
                        reserveSpaceForSubtitle: true
                        icon.name: qsDel.iconName
                        Layout.fillWidth: true
                    }

                    Loader {
                        id: lkControls
                        active: false

                        sourceComponent: RowLayout {
                            visible: false
                            VisibleBehavior on visible {}

                            Component.onCompleted: visible = true

                            PlayButton {
                                visible: qsDel.value.length > 1
                                onClicked: {
                                    zoneView.currentPlayer.searchAndPlayNow('[%1]="%2"'.arg(qsDel.field).arg(qsDel.value)
                                                                            , autoShuffle)
                                    event.queueCall(250, () => mainView.currentIndex = 1 )
                                }
                            }
                            AppendButton {
                                visible: qsDel.value.length > 1
                                onClicked: {
                                    zoneView.currentPlayer
                                    .searchAndAdd('[%1]="%2"'.arg(qsDel.field).arg(qsDel.value)
                                                  , false, autoShuffle)
                                }
                            }
                            ShowButton {
                                visible: qsDel.value.length > 1
                                onClicked: {
                                    let obj = {}
                                    obj[qsDel.field] = '"%1"'.arg(qsDel.value)
                                    trackView.search(obj)
                                }
                            }
                        }
                    }
                }

                onHoveredChanged: {
                    if (!hovered) {
                        qsTimer.stop()
                        lkControls.active = false
                    } else {
                        qsTimer.start()
                    }
                }

                Timer {
                    id: qsTimer
                    interval: Kirigami.Units.shortDuration
                    onTriggered: lkControls.active = hovered
                }

            }
        }
    }

    Loader {
        id: globalMenu
        active: false

        sourceComponent: Menu {
            Component.onCompleted: popup()

            MenuItem {
                text: 'Configure…'
                icon.name: 'configure'
                onTriggered: configAction.trigger()
            }
            MenuSeparator {}

            MenuItem {
                text: 'Set Search Fields…'
                icon.name: "search"
                enabled: mcws.isConnected
                onTriggered: fieldsComp.createObject(root)
            }
            MenuSeparator {}
            MenuItem { action: mcws.clearAllZones; enabled: mcws.isConnected }
            MenuItem { action: mcws.stopAllZones; enabled: mcws.isConnected }
            MenuSeparator {}
            MenuItem {
                enabled: mcws.isConnected && (!splasher || (splasher && splasher.screenSaverMode))
                text: (splasher && splasher.screenSaverMode ? 'Stop' : 'Start') + ' Screensaver'
                icon.name: splasher && splasher.screenSaverMode ? 'process-stop-symbolic' : 'preferences-desktop-screensaver-symbolic'
                onTriggered: plasmoidConn.onToogleScreenSaver()
            }
            MenuSeparator {}
            MenuItem {
                text: 'Refresh View'
                icon.name: 'view-refresh'
                enabled: mcws.isConnected
                onTriggered: mcws.reset()
            }
            MenuItem {
                text: 'Close Connection'
                icon.name: 'network-disconnect-symbolic'
                enabled: mcws.isConnected
                onTriggered: mcws.closeConnection()
            }
            MenuSeparator {}
            MenuItem {
                text: plasmoidRoot.hideOnWindowDeactivate
                        ? 'Pin to Desktop'
                        : 'Unpin from Desktop'
                icon.name: plasmoidRoot.hideOnWindowDeactivate
                            ? "window-pin"
                            : 'window-unpin'
                onTriggered: plasmoidRoot.hideOnWindowDeactivate = !plasmoidRoot.hideOnWindowDeactivate
            }
        }

    }

    Component {
        id: sendToComp

        Menu {

            Component.onCompleted: popup()
            onClosed: destroy()

            Repeater {
                model: mcws.zoneModel

                delegate: MenuItem {
                    required property string zonename
                    required property int index

                    text: 'To: ' + zonename
                    visible: !zoneView.isCurrent(index)
                    icon.name: 'view-media-track-symbolic'
                    onTriggered: {
                        mcws.sendListToZone(trackView.searchMode
                                            ? trackView.showingPlaylist
                                              ? mcws.playlists.trackModel.items
                                              : searcher.items
                                            : zoneView.currentZone.trackList.items
                                            , index)
                    }
                }
            }
        }
    }

    Component {
        id: fieldsComp

        Kirigami.Dialog {
            focus: true
            title: "Select Search Fields"

            Component.onCompleted: {
                fields.model = ''
                fields.model = searcher.mcwsFields
                visible = true
            }
            onClosed: destroy()

            preferredWidth: Math.ceil(root.width/3)
            preferredHeight: root.height

            showCloseButton: false
            standardButtons: Kirigami.Dialog.Reset | Kirigami.Dialog.Close

            onReset: {
                fields.model = ''
                searcher.init()
                fields.model = searcher.mcwsFields
            }

            ListView {
                id: fields
                clip: true

                delegate: PComp.ToolButton {
                    required property string field

                    text: field
                    implicitWidth: fields.width
                    checkable: true
                    checked: searcher.searchFields.hasOwnProperty(field)
                    onClicked: {
                        if (checked)
                            searcher.searchFields[field] = ''
                        else
                            delete searcher.searchFields[field]
                    }
                }
            }
        }

    }

    footer: RowLayout {
        visible: mcws.isConnected
        height: Kirigami.Units.iconSizes.medium
        Layout.margins: 0

        Item {
            Layout.fillWidth: true

            PageIndicator {
                id: pi
                count: mainView.count
                visible: mcws.isConnected
                currentIndex: mainView.currentIndex
                anchors.centerIn: parent

                delegate: Rectangle {
                    required property int index

                    implicitWidth: Kirigami.Units.iconSizes.small
                    implicitHeight: Kirigami.Units.iconSizes.small

                    radius: width / 2
                    color: Kirigami.Theme.highlightColor

                    opacity: index === pi.currentIndex ? 1.0 : 0.4

                    Behavior on opacity {
                        PropertyAnimation { duration: 500 }
                    }

                    MouseArea {
                        anchors.fill: parent
                        onClicked: mainView.currentIndex = index
                    }
                }
            }
        }

        PComp.ToolButton {
            icon.name: 'send-to-symbolic'
            icon.width: Kirigami.Units.iconSizes.small
            icon.height: Kirigami.Units.iconSizes.small

            visible: mainView.currentIndex === 1
                     && mcws.zoneModel.count > 1
                     && trackView.count > 0

            PComp.ToolTip {
                text: 'Send the Current Playlist'
            }

            onClicked: sendToComp.createObject(root)
        }

        PComp.ToolButton {
            icon.name: 'view-media-track-symbolic'
            icon.width: Kirigami.Units.iconSizes.small
            icon.height: Kirigami.Units.iconSizes.small

            visible: mainView.currentIndex === 1
                     && trackView.count > 0

            PComp.ToolTip {
                text: 'Show track currently playing'
            }

            onClicked: trackView.highlightPlayingTrack()
        }

        ListPositioner {
            id: listPositioner
            Layout.alignment: Qt.AlignRight

        }
    }
}
