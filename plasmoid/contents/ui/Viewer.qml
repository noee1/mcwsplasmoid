import QtQuick
import org.kde.kirigami as Kirigami

ListView {
    id: list

    property bool useHighlight: true
    readonly property var modelItem: !model || currentIndex === -1
                            ? undefined
                            : model.get(currentIndex)

    Component {
        id: hl
        Rectangle {
                width: list.width
                color: Kirigami.Theme.highlightColor
                radius: 5
                y: list.currentItem ? list.currentItem.y : -1
                Behavior on y {
                    SpringAnimation {
                        spring: 2.5
                        damping: 0.3
                    }
                }
            }
    }

    spacing: Kirigami.Units.smallSpacing
    clip: true
    highlightMoveDuration: 1
    highlight: useHighlight ? hl : null
}
