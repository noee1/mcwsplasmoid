import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import org.kde.kcmutils
import '../helpers'

SimpleKCM {

    property alias cfg_updateInterval: updateInterval.value
    property alias cfg_hostConfig: configMcws.hostConfig
    property alias cfg_autoConnect: autoConnect.checked

    ColumnLayout {
        anchors.fill: parent

        ConfigMcws {
            id: configMcws
            Layout.fillHeight: true
        }

        GroupSeparator { text: 'Options' }

        RowLayout {
            CheckBox {
                id: autoConnect
                Layout.fillWidth: true
                text: 'Maintain Host Connection (auto connect)'
            }
            Label {
                text: i18n('Update interval:')
                Layout.alignment: Qt.AlignRight
            }
            FloatSpinner {
                id: updateInterval
                decimals: 1
            }

        }
    }


}
