import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import org.kde.plasma.plasmoid
import org.kde.kcmutils
import org.kde.kirigami as Kirigami
import '../helpers'

SimpleKCM {
    id: root

    property alias cfg_showVolumeSlider: showVolSlider.checked
    property alias cfg_abbrevTrackView: abbrevTrackView.checked
    property alias cfg_advancedTrayView: advTrayView.checked
    property alias cfg_showStopButton: showStopButton.checked
    property alias cfg_useImageIndicator: imgIndicator.checked
    property alias cfg_useImageBackground: imgBackground.checked
    property alias cfg_thumbSize: thumbSize.value
    property alias cfg_highQualityThumbs: hqCoverArt.checked
    property alias cfg_rightJustify: rightJustify.checked
    property alias cfg_scrollTrack: scrollTrack.checked

    property alias cfg_useTheme: useTheme.checked
    property alias cfg_themeName: theme.displayText
    property alias cfg_themeDark: themeDark.checked

    property alias cfg_trayViewSize: compactSize.value
    property alias cfg_useZoneCount: useZoneCount.checked

    Kirigami.FormLayout {

        GridLayout {
            columns: 2
            columnSpacing: Kirigami.Units.largeSpacing

            RowLayout {
                Label {
                    text: 'Thumbnail Size'
                }
                Slider {
                    id: thumbSize
                    Layout.preferredWidth: Math.round(root.width /4)
                    from: Kirigami.Units.iconSizes.medium
                    to: Kirigami.Units.iconSizes.enormous
                }
            }
            CheckBox {
                id: hqCoverArt
                text: 'High Quality Cover Art'
            }

            CheckBox {
                id: showVolSlider
                text: "Show Volume Slider"
            }
            CheckBox {
                id: abbrevTrackView
                text: "Abbreviated Track View"
            }
            Item {
               Layout.columnSpan: 2
               Layout.topMargin: Kirigami.Units.gridUnit
            }
        }

        // Theme obj def'n {name, canStyle, c1, c2}
        FormSeparator {}
        RowLayout {
            Switch {
                id: useTheme
                text: "Color Theming"
                font.pointSize: Kirigami.Theme.defaultFont.pointSize + 2
            }

            ComboBox {
                id: theme
                enabled: useTheme.checked
                textRole: 'name'
                model: ListModel {id: lm}
                onActivated: {
                    cfg_themeName = currentText

                }
                Component.onCompleted: {
                    JSON.parse(Plasmoid.configuration.themes)
                        .forEach(t => { lm.append(t) })
                }
            }

            CheckBox {
                id: themeDark
                text: 'Dark'
                enabled: useTheme.checked
            }

        }

        Label {}

        FormSeparator {}
        Switch {
            id: advTrayView
            text: "Advanced Panel View (only in horizontal panels)"
            font.pointSize: Kirigami.Theme.defaultFont.pointSize + 2
        }

        FormSpacer {}

        GridLayout {
            visible: advTrayView.checked
            columns: 2
            columnSpacing: Kirigami.Units.largeSpacing * 4

            CheckBox {
                id: useZoneCount
                text: "Size to Number of Zones"
            }
            RowLayout {
                opacity: !useZoneCount.checked
                Label {
                    text: 'Absolute Size'
                }

                Slider {
                    id: compactSize
                    Layout.preferredWidth: Math.round(root.width /4)
                    from: 15
                    to: 60
                }
            }

            Item {
               Layout.columnSpan: 2
               Layout.topMargin: Kirigami.Units.gridUnit
            }

            CheckBox {
                id: imgIndicator
                text: "Playback Indicator"
            }
            CheckBox {
                id: showStopButton
                text: "Show Stop Button"
            }
            CheckBox {
                id: imgBackground
                text: "Themed Panel Background"
            }
            CheckBox {
                id: rightJustify
                text: "Right Justify Panel"
            }
            CheckBox {
                id: scrollTrack
                text: "Scroll Long Track Names"
            }
        }

    }
}
