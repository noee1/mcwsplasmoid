import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import org.kde.kirigami as Kirigami
import org.kde.kcmutils
import "../helpers"

pragma ComponentBehavior: Bound

ScrollViewKCM {
    property alias cfg_defaultFields: lm.outputStr

    // defn: { "field": "Name", "sortable": true, "searchable": true, "mandatory": true }
    ConfigListModel {
        id: lm
        configKey: 'defaultFields'
    }

    component FieldItemDelegate: Item {
        id: fid
        width: ListView.view.width
        implicitHeight: fieldItem.implicitHeight

        required property int index
        required property string field
        required property bool sortable
        required property bool searchable

        ItemDelegate {
            id: fieldItem
            width: fid.width

            onClicked: fields.currentIndex = fid.index

            contentItem: RowLayout {
                Kirigami.ListItemDragHandle {
                    listItem: fieldItem
                    listView: fields
                    onMoveRequested: (oldIndex, newIndex) => lm.items.move(oldIndex, newIndex, 1)
                }

                Label {
                    text: fid.field
                    Layout.fillWidth: true
                }

                CheckBox {
                    text: 'Sortable'
                    checked: fid.sortable
                    onClicked: {
                        lm.items.setProperty(fid.index, 'sortable', checked)
                        lm.items.save()
                    }
                }

                CheckBox {
                    text: 'Searchable'
                    checked: fid.searchable
                    onClicked: {
                        lm.items.setProperty(fid.index, 'searchable', checked)
                        lm.items.save()
                    }
                }
            }
        }
    }

    ListView {
        id: fields
        model: lm.items
        clip: true
        spacing: 0
        anchors.fill: parent

        moveDisplaced: Transition {
            YAnimator {
                duration: Kirigami.Units.longDuration
                easing.type: Easing.InOutQuad
            }
        }

        delegate: FieldItemDelegate {}
    }
}
