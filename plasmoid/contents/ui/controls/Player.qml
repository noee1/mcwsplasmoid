import QtQuick
import QtQuick.Layouts
import org.kde.kirigami as Kirigami

// playback controls for use in a delegate
Item {
    id: root
    implicitHeight: rl.height

    property bool showVolumeSlider: true
    property bool showStopButton: true

    RowLayout {
        id: rl
        width: root.width
        spacing: Kirigami.Units.smallSpacing

        PrevButton {}
        PlayPauseButton {}
        StopButton { visible: showStopButton }
        NextButton {}

        Item {
            visible: showVolumeSlider
            Layout.preferredWidth: Kirigami.Units.largeSpacing
        }

        VolumeControl {
            Layout.fillWidth: true
            showSlider: showVolumeSlider
        }
    }
}

