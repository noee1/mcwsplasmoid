import QtQuick
import QtQuick.Layouts
import org.kde.kirigami as Kirigami
import org.kde.plasma.components as PComp
import '..'

Item {
    id: root
    implicitWidth: rl.width
    implicitHeight: rl.height

    property bool horizontalList: false
    property Viewer list

    visible: list

    RowLayout {
        id: rl
        spacing: 0

        PComp.ToolButton {
            icon.name: root.horizontalList ? 'go-first' : 'go-top'
            icon.width: Kirigami.Units.iconSizes.small
            icon.height: Kirigami.Units.iconSizes.small
            onClicked: root.list.positionViewAtBeginning()

            PComp.ToolTip {
                text: 'Beginning of list'
            }
        }

        PComp.ToolButton {
            icon.name: root.horizontalList ? 'go-last' : 'go-bottom'
            icon.width: Kirigami.Units.iconSizes.small
            icon.height: Kirigami.Units.iconSizes.small
            onClicked: root.list.positionViewAtEnd()

            PComp.ToolTip {
                text: 'End of list'
            }
        }
    }
}

