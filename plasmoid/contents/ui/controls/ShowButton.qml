import QtQuick
import org.kde.plasma.components as PComp

PComp.ToolButton {
    icon.name: 'search'
    property alias tipText: tt.text
    PComp.ToolTip { id: tt; text: 'Show Details' }
}
