import QtQuick
import QtQuick.Controls

ToolButton {
    id: root
    icon.name: 'media-playlist-repeat'

    onClicked: menuComp.createObject(root)

    ToolTip {
        text: 'Repeat Mode'
    }

    Component {
        id: menuComp

        Menu {
            id: repeatMenu

            onAboutToShow: player.getRepeatMode()

            Component.onCompleted: popup()

            Repeater {
                model: player.repeatModes
                delegate: MenuItem {
                    action: modelData
                    autoExclusive: true
                }
            }
        }
    }

}
