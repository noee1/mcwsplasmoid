import QtQuick
import QtQuick.Controls
import '../models'

ToolButton {
    id: sorter
    icon.name: "playlist-sort"

    property bool showSort: true

    // sort menu is derived from fields in the searcher model
    // sort role stored in Searcher::sortField
    property Searcher target

    onTargetChanged: {
        if (_sortMenu) {
            _sortMenu.destroy()
            _sortMenu = null
        }
    }

    property Menu _sortMenu

    onClicked: {
        if (!_sortMenu) {
            _sortMenu = menuComp.createObject(sorter)
            target.mcwsFields.forEach(f => {
                if (f.sortable)
                    _sortMenu.addItem(mi.createObject(sorter, {text: f.field}))
            })
        }
        _sortMenu.popup()
    }

    text: showSort
          ? sorter.target ? sorter.target.sortField : ''
          : ''
    hoverEnabled: true

    ToolTip {
        text: 'Sort Tracks'
    }

    Component {
        id: menuComp

        Menu {

            MenuItem {
                text: 'No Sort'
                checkable: true
                autoExclusive: true
                checked: sorter.target
                         ? sorter.target.sortField === ""
                         : true
                onTriggered: sorter.target.sortField = ""
            }
        }
    }

    Component {
        id: mi

        MenuItem {
            autoExclusive: true
            checkable: true
            checked: text === sorter.target.sortField
            onTriggered: sorter.target.sortField = text
        }
    }
}
