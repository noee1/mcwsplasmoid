import QtQuick
import org.kde.plasma.components as PComp

PComp.ToolButton {
    icon.name: "media-playback-start"
    property alias tipText: tt.text
    PComp.ToolTip { id: tt; text: 'Play Now' }
}
