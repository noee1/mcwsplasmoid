import QtQuick
import org.kde.kirigami as Kirigami
import '../helpers'
import '..'

Item {
    id: root

    implicitHeight: ti.height
    implicitWidth: ti.width

    property bool animateLoad: true
    property string sourceKey: ''
    property int duration: 750
    property bool thumbnail: false

    readonly property alias image: ti
    property alias sourceSize: ti.sourceSize

    property alias color: shadowRectangle.color
    property alias radius: shadowRectangle.radius
    property alias shadow: shadowRectangle.shadow
    property alias border: shadowRectangle.border
    property alias corners: shadowRectangle.corners

    // Interface helper component
    property McwsImageUtils imageUtils

    onSourceKeyChanged: {
        // Guard the sourceKey
        if (sourceKey === undefined
                || sourceKey.length === 0
                || sourceKey === '-1') {
            ti.source = imageUtils.defaultImage
        } else {
            ti.source = imageUtils.getImageUrl(sourceKey, thumbnail)
        }
    }

    signal animationStart()
    signal animationEnd()

    FadeBehavior on sourceKey {
        enabled: root.animateLoad
        fadeDuration: root.duration
        onAnimationStart: root.animationStart()
        onAnimationEnd: root.animationEnd()
    }

    Image {
        id: ti

        fillMode: Image.PreserveAspectFit
        mipmap: true

        onStatusChanged: {
            if (status === Image.Error) {
                if (typeof root.imageUtils.setImageError === 'function')
                    root.imageUtils.setImageError(root.sourceKey)
                source = root.imageUtils.defaultImage
            }
        }

    }

    ShaderEffectSource {
        id: textureSource
        sourceItem: ti
        hideSource: !shadowRectangle.softwareRendering
    }

    Kirigami.ShadowedTexture {
        id: shadowRectangle

        color: 'transparent'

        shadow.size: 20
        shadow.xOffset: 5
        shadow.yOffset: 5

        border.width: 0
        border.color: Kirigami.Theme.textColor

        corners.topLeftRadius: 4
        corners.topRightRadius: 4
        corners.bottomLeftRadius: 4
        corners.bottomRightRadius: 4

        anchors.fill: parent
        source: (ti.status === Image.Ready && !softwareRendering) ? textureSource : null
    }
}

