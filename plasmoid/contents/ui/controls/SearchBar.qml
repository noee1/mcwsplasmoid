import QtQuick
import QtQuick.Layouts
import org.kde.kirigami as Kirigami
import org.kde.plasma.components as PComp

pragma ComponentBehavior: Bound

RowLayout {
    spacing: 0
    property ListView list
    property string role: ''
    property string currentSelection: ''

    function reset() {
        currentSelection = ''
        list.positionViewAtBeginning()
    }

    function scrollList(val) {
        const i = list.model.findIndex(item => val === item[role].slice(0,1))
        if (i !== -1) {
            list.positionViewAtIndex(i, ListView.Center)
            list.currentIndex = i
            currentSelection = val
        }
    }

    function scrollCurrent() {
        if (currentSelection !== '')
            scrollList(currentSelection)
    }

    Repeater {
        id: btns
        model: letters.length
        readonly property string letters: "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

        delegate: PComp.ToolButton {
            required property int index

            text: btns.letters.slice(index,index+1)
            onClicked: scrollList(text)
            checkable: true
            autoExclusive: true
            height: Kirigami.Units.smallSpacing
            width:  Kirigami.Units.smallSpacing
        }
    }
}
