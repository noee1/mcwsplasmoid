import QtQuick
import QtQuick.Layouts
import org.kde.plasma.components as PComp
import org.kde.kirigami as Kirigami
import '..'
import '../helpers'

RowLayout {
    id: root
    spacing: Kirigami.Units.smallSpacing

    property alias showLabel: pnPosLabel.visible
    property alias showSlider: trackPos.visible

    Timer {
        repeat: true
        interval: 1000
        running: plasmoidRoot.expanded
                 && model.state === PlayerState.Playing
        triggeredOnStart: true

        onTriggered: {
            if (!trackPos.disablePosUpdate)
                trackPos.value = model.positionms / 10000
        }
    }

    PComp.Label {
        visible: root.showSlider
        font: Kirigami.Theme.smallFont
        text: model.elapsedtimedisplay ?? ''
    }

    PComp.Slider {
        id: trackPos

        property bool disablePosUpdate: false

        from: 0
        to: model.durationms / 10000
        value: 0
        Layout.fillWidth: true

        onPressedChanged: {
            if (!pressed) {
                player.setPlayingPosition(position*to*10000)
                event.queueCall(500, () => disablePosUpdate = false)
            }
            else
                disablePosUpdate = true
        }

        VisibleBehavior on visible {}
    }

    PComp.Label {
        visible: root.showSlider
        font: Kirigami.Theme.smallFont
        text: model.remainingtimedisplay ?? ''
    }

    PComp.Label {
        id: pnPosLabel
        font: Kirigami.Theme.smallFont
        text: model.playingnowpositiondisplay
              ? "[%1]".arg(model.playingnowpositiondisplay)
              : ''
    }
}
