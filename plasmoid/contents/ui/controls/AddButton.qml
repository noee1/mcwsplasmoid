import QtQuick
import org.kde.plasma.components as PComp

PComp.ToolButton {
    icon.name: 'media-playlist-consecutive-symbolic'
    property alias tipText: tt.text
    PComp.ToolTip { id: tt; text: 'Add Next to Play' }
}
