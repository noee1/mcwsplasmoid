import QtQuick
import org.kde.plasma.components as PComp

PComp.ToolButton {
    action: model.player.stop
    enabled: model.playingnowtracks > 0
}
