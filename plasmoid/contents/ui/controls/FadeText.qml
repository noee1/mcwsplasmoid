import QtQuick
import org.kde.plasma.components as PComp
import org.kde.kirigami as Kirigami
import '../helpers'

PComp.Label {
    id: txt
    property int duration: Kirigami.Units.longDuration * 2
    property alias animate: fb.enabled

    FadeBehavior on text {
        id: fb
        fadeDuration: txt.duration
    }

}
