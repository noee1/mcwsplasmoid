import QtQuick
import QtQuick.Controls

ToolButton {
    id: root
    icon.name: 'media-playlist-shuffle'

    onClicked: menuComp.createObject(root)

    ToolTip {
        text: 'Shuffle Mode'
    }

    Component {
        id: menuComp

        Menu {
            id: shuffleMenu

            onAboutToShow: player.getShuffleMode()

            Component.onCompleted: popup()

            MenuItem {
                action: player.shuffle
            }
            MenuSeparator {}
            Repeater {
                model: player.shuffleModes
                delegate: MenuItem {
                    action: modelData
                    autoExclusive: true
                }
            }
        }
    }

}
