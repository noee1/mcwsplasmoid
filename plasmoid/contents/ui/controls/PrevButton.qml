import QtQuick
import org.kde.plasma.components as PComp

PComp.ToolButton {
    action: model.player.previous
    enabled: model.playingnowposition > 0
}
