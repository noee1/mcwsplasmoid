import QtQuick
import org.kde.plasma.components as PComp

PComp.ToolButton {
    action: model.player.next
    enabled: model.nextfilekey !== -1
}
