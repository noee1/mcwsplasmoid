import QtQuick
import org.kde.plasma.components as PComp

PComp.ToolButton {
    icon.name: 'media-playlist-append'
    property alias tipText: tt.text
    PComp.ToolTip { id: tt; text: 'Append to Playing Now' }
}
